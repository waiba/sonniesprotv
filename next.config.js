const path = require('path')
module.exports = {
    async redirects() {
        return [
            // if the host is `example.com`,
            // this redirect will be applied
            {
                source: "/13.210.196.165/",
                destination: "https://sprotv.com.au/",
                permanent: true
            },
            {
                source: "/home/",
                destination: "https://sprotv.com.au/",
                permanent: true
            },
            {
                source: "/sonniesprotv.com.au/",
                destination: "https://sprotv.com.au/",
                permanent: true
            },
            //                {
            //                      source: "/sonniesprotv.com.au/services/",
            //                      destination: "https://sprotv.com.au/services/",
            //                      permanent: true
            //                },
            //                {
            //                      source: "/sonniesprotv.com.au/services/:slug/",
            //                      destination: "https://sprotv.com.au/services/:slug/",
            //                      permanent: true
            //                },
            {
                source: "/sonniesprotv.com.au/:slug*/",
                destination: "https://sprotv.com.au/:slug*/",
                permanent: true
            }
        ]
    },
    trailingSlash: true,
    images: {
        domains: ['scontent.cdninstagram.com', 'sonnies.sprotv.com.au']
    },
    webpackDevMiddleware: config => {
        config.watchOptions = {
            poll: 1000,
            aggregateTimeout: 300
        }

        return config
    },
    sassOptions: {
        includePaths: [path.join(__dirname, 'styles')]
    }
}