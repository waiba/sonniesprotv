import Link from "next/link"
import ContactForm from "./ContactForm"

import styles from './css/ContactBody.module.css'

const ContactBody = ({contactForm, homeContactInfo}) => {

    return (
        <div className={`${styles.contactBodySection} container-lg container-fluid px-4 px-md-5 px-lg-0 `}>
            <div className="row d-md-flex align-items-stretch">
                <div className="col-md-4">
                    <div className="text-center col-md-12 py-md-5 py-4 px-md-4 px-4 border mb-md-5 mb-4">
                        <img src="/chat_bubble_outline.png" className="pb-md-4" />
                        <h4 className={`mb-md-4 ${styles.sidebarTitle}`}>{contactForm?.chatOnlineTitle}</h4>
                        <p>{contactForm?.chatOnlineContent}</p>
                        <Link href="https://m.me/sonniesprotv">
                            <a target="_blank" onClick={
                                async (e) => {
                                        //everything is fully loaded, don't use me if you can use DOMContentLoaded
                                    //     window.fbAsyncInit = function(e) {
                                    //     FB.init({
                                    //       xfbml            : true,
                                    //       version          : 'v9.0'
                                    //     });
                                    //   };

                                    //   (function(d, s, id) {
                                    //   var js, fjs = d.getElementsByTagName(s)[0];
                                    //   if (d.getElementById(id)) return;
                                    //   js = d.createElement(s); js.id = id;
                                    //   js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
                                    //   fjs.parentNode.insertBefore(js, fjs);
                                    // }(document, 'script', 'facebook-jssdk'));
                                }
                            } className="btn callNow primaryButton mt-md-3">{contactForm?.chatOnlineButtonText}</a>
                        </Link>
                    </div>
                    <div className="text-center col-md-12 px-md-4 py-md-5 border py-4 px-4 mb-4 ">
                        <img src="/phone_in_talk.png" className="pb-md-4" />
                        <h4 className={`mb-md-4 ${styles.sidebarTitle}`}>{contactForm?.callOurExpertTitle}</h4>
                        <p>{contactForm?.callOurExpertContent}</p>
                        <Link href={`tel:${homeContactInfo?.phone}`}>
                            <button className="btn primaryButton mt-md-3">{contactForm?.callOurExpertButtonText}</button>
                        </Link>
                    </div>
                </div>
                <div className="col-md-8 px-md-5 d-md-flex align-items-stretch ">
                    <div className="col-md-12 border ">
                        <div className="col-md-10  py-md-5 py-4  m-auto px-4 px-md-0">
                            <h4 className={`mb-4 ${styles.sidebarTitle}`}>Contact Form</h4>
                            <ContactForm />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ContactBody;
