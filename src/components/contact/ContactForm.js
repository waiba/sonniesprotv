import React, { useRef, useState, useEffect } from 'react';
import { Helmet } from 'react-helmet'
import fetch from 'isomorphic-unfetch';
import { isEmpty } from 'lodash'
import { InputGroup, InputGroupAddon, InputGroupText, Input, Form, Row, Col, FormGroup, Label, FormFeedback, FormText } from 'reactstrap';
import styles from './css/ContactForm.module.css'
import ReCAPTCHA from "react-google-recaptcha"

const ContactForm = () => {


    const [values, setValues] = useState({
        fullName: '',
        fullNameError: false,
        fullNameSuccess: false,
        email: '',
        emailSuccess: false,
        emailError: false,
        category: '',
        categoryError: false,
        categorySucces: false,
        phoneNumber: '',
        phoneError: false,
        phoneSuccess: false,
        contactMessage: '',
        messageError: false,
        messageSuccess: false,
        captchaToken: '',
        captchaTokenError: false,
        formSuccess: false,
        formMessage: '',
        reload: false
    });

    const { fullName, fullNameError, fullNameSuccess, email, emailError, emailSuccess, category, categorySucces, categoryError, phoneNumber, phoneError, phoneSuccess, contactMessage, captchaToken, captchaTokenError, messageError, messageSuccess, formSuccess, formMessage, reload } = values;

    const handleChangeEmail = e => {
        setValues({ ...values, email: e.target.value, emailError: false, emailSuccess: false })
    }

    const handleChangeName = e => {
        setValues({ ...values, fullName: e.target.value, fullNameError: false, fullNameSuccess: false })
    }

    const handleChangePhone = e => {
        setValues({ ...values, phoneNumber: e.target.value, phoneError: false, phoneSuccess: false })
    }

    const handleChangeMessage = e => {
        setValues({ ...values, contactMessage: e.target.value, messageError: false, messageSuccess: false })
    }

    const handleChangeCategory = e => {
        if (e.target.value == '') {
            setValues({ ...values, category: false, categoryError: true, categorySucces: false })
        } else {
            setValues({ ...values, category: e.target.value, categoryError: false, categorySucces: false })
        }
    }

    const onChangeCaptcha = value => {
        setValues({ ...values, captchaToken: value })
    }

    const showError = () => {
        if (fullNameError) {
            return <p className="text-danger">{fullNameError}</p>
        }
        if (emailError) {
            return <p className="text-danger">{emailError}</p>
        }
        if (phoneError) {
            return <p className="text-danger">{phoneError}</p>
        }
        if (messageError) {
            return <p className="text-danger">{messageError}</p>
        }
        if (categoryError) {
            return <p className="text-danger">{categoryError}</p>
        }
        if (captchaTokenError) {
            return <p className="text-danger">{captchaTokenError}</p>
        }
    }

    // const formMessage = () => {
    //     if(formSuccess){
    //         return <p className="text-danger">Message Sent</p>
    //     }
    //     else {
    //         return <p className="text-danger">Something went wrong</p>
    //     }
    // }



    //const toast = useToast();
    //const { colorMode } = useColorMode();
    const bgColor = {
        light: 'blue.50',
        dark: 'blue.900'
    };
    const borderColor = {
        light: 'blue.200',
        dark: 'blue.900'
    };

    const formSubmit = async (e) => {
        e.preventDefault();
        setValues({ ...values, formSuccess: "" })
        if (isEmpty(email)) {
            //console.log('empty valye');
            setValues({ ...values, emailError: "Please enter the valid email address" })
            return;
        }

        if (isEmpty(fullName)) {
            //console.log('empty valye');
            setValues({ ...values, fullNameError: "Name field is required" })
            return;
        }

        if (isEmpty(phoneNumber)) {
            //console.log('empty valye');
            setValues({ ...values, phoneError: "Phone number is required" })
            return;
        }

        if (isEmpty(category)) {
            setValues({ ...values, categoryError: "Category is required" })
            return;
        }

        if (isEmpty(contactMessage)) {
            setValues({ ...values, messageError: "Message is required" })
            return;
        }

        if (isEmpty(captchaToken)) {
            setValues({ ...values, captchaTokenError: "Google Captcha error" })
            return;
        }

        // if(!isEmpty(captchaToken)) {
        //     setValues( { ...values, captchaTokenError: false} )
        //     return;
        // }


        const res = await fetch('/api/contactform', {
            body: JSON.stringify({
                fullName: fullName,
                phoneNumber: phoneNumber,
                category: category,
                contactMessage: contactMessage,
                phoneNumber: phoneNumber,
                email: email,
                captchaToken: captchaToken
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        }).then(function (response) {
            return response.json();
        });;

        const { error } = res?.error;
        const message = res?.message;



        if (!isEmpty(res?.error)) {
            //console.log(res?.error);
            setValues({ ...values, formSuccess: false, formMessage: res?.error })
            return;

        } else {
            if (res?.message?.status == "mail_failed") {
                setValues({ ...values, formSuccess: false, formMessage: res?.message?.message })
                return;
            } else if (res?.message?.status == "validation_failed") {
                setValues({ ...values, formSuccess: false, formMessage: res?.message?.message })
                return;
            }

            if (res?.message?.status == "mail_sent") {
                setValues({
                    ...values, formMessage: res?.message?.message, formSuccess: true,
                    fullName: '',
                    fullNameError: false,
                    fullNameSuccess: false,
                    email: '',
                    emailSuccess: false,
                    emailError: false,
                    category: '',
                    categoryError: false,
                    categorySucces: false,
                    phoneNumber: '',
                    phoneError: false,
                    phoneSuccess: false,
                    contactMessage: '',
                    messageError: false,
                    messageSuccess: false,
                    captchaToken: '',
                    captchaTokenError: false,
                    reload: false
                })
                return;
            }
            //   setValues( { ...values, formMessage:'Form succesfully submitted', formSuccess: true } )
            //   return;

        }



    };


    return (
        <div>
            {/* <Helmet>
                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
            </Helmet> */}
            <Form className="pt-3">
                <Col md={12} className="pb-md-4 pb-3">
                    <FormGroup>
                        <Input placeholder="Full Name" type="text" onChange={handleChangeName} value={fullName} required className={`${styles.fieldStyle}`} />
                    </FormGroup>
                </Col>
                <Col md={12} className="pb-md-4 pb-3">
                    <FormGroup>
                        <Input placeholder="Email" type="email" onChange={handleChangeEmail} value={email} required className={`${styles.fieldStyle}`} />
                    </FormGroup>
                </Col>

                <Col md={12} className="pb-md-4 pb-3">
                    <FormGroup>
                        <Input placeholder="Phone Number" type="text" onChange={handleChangePhone} value={phoneNumber} required className={`${styles.fieldStyle}`} />
                    </FormGroup>
                </Col>

                <Col md={12} className="pb-md-4 pb-3"  >
                    <FormGroup>
                        <Input type="select" name="select" id="exampleSelect" onChange={handleChangeCategory} className={`${styles.fieldStyle}`}>
                            <option value="">Category</option>
                            <option>TV Wall Mount</option>
                            <option>Floating Shelves</option>
                            <option>Soundbar Installation</option>
                            <option>Antenna Installation</option>
                            <option>CCTV Installation</option>
                            <option>Others</option>
                        </Input>
                    </FormGroup>
                </Col>
                <Col md={12} className="pb-md-4 pb-3">
                    <FormGroup>
                        <Input type="textarea" name="text" id="exampleText" onChange={handleChangeMessage} value={contactMessage} rows="4" className={`${styles.fieldStyle}`} placeholder="Write Message" />
                    </FormGroup>
                </Col>

                <ReCAPTCHA sitekey={process.env.NEXT_PUBLIC_GOOGLE_CAPTCHA_SITE_KEY} onChange={onChangeCaptcha} />

                {showError()}
                <br />
                <InputGroup>
                    <button className="btn primaryButton mt-3" onClick={formSubmit} >
                        Submit
                    </button>
                </InputGroup>
                {
                    formSuccess == true &&
                    <p>{formMessage}</p>
                }
                {
                    formSuccess == false &&
                    <p>{formMessage}</p>
                }


            </Form>
        </div>
    )
}

export default ContactForm;
