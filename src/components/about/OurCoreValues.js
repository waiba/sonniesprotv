import Image from 'next/image';
import styles from './css/OurCoreValues.module.css'

const OurCoreValues = ({ acfAboutOurCoreValues }) => {
    return (
        <div className={`${styles.ourCoreValuesSection} container-lg container-fluid px-md-5 px-4 px-lg-0`}>
            <div className="col-md-12 col-xl-8 col-lg-10 m-auto text-center">
                <h3 className="sectionTitle pb-4">{acfAboutOurCoreValues?.coreValuesMainTitleText}</h3>
                <p className="sectionMainContent  mb-5">{acfAboutOurCoreValues?.coreValuesMainContent}</p>
            </div>
            <div className="row">
                {
                    acfAboutOurCoreValues?.coreValuesSingleBlocksWrap.map((value, key) => {
                        return (
                            <div className="col-md-4 text-center px-4 px-lg-5 pb-md-0 pb-5" key={key}>
                                {/* <img src={value?.singleIcon?.sourceUrl} alt={value?.singleIcon?.altText} className={styles.singleImage} /> */}
                                <div className={styles.singleImage}>
                                    <Image
                                        src={value?.singleIcon?.sourceUrl}
                                        // just put the original width and height of the original image, in order to provide the right aspect ratio
                                        // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                        width={72}
                                        height={72}
                                        // layout="fill"
                                        // objectFit="contain"
                                        alt={value?.singleIcon?.altText} />
                                </div>
                                <h5 className={`${styles.singleTitle} pb-4`}>{value?.singleTitle}</h5>
                                <p className={styles.singleContent}>{value?.singleContent}</p>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}
export default OurCoreValues;
