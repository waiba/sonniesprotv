import Image from 'next/image';
import styles from './css/AboutpageWhyChooseUs.module.css'

const AboutpageWhyChooseUs = ({ acfAboutPageWhyChooseUs }) => {
    return (
        <div className={`${styles.aboutWhyChooseusSection} container-lg container-fluid px-md-5 px-4 px-lg-0`}>
            <div className="row d-flex">
                <div className="col-xxl-5 offset-xxl-1 col-xl-6 col-lg-6 col-md-6 pe-lg-5 pe-md-2 mt-lg-5 mt-md-0 order-last order-md-first">
                    <p className="smallSubTitle">{acfAboutPageWhyChooseUs?.whyChooseUsSmallTitleText}</p>
                    <h3 className="sectionTitle pb-4">{acfAboutPageWhyChooseUs?.whyChooseUsMainTitleText}</h3>

                    <div dangerouslySetInnerHTML={{ __html: acfAboutPageWhyChooseUs?.whyChooseUsMainContent }} className="sectionMainContent why-choose-us-desc  pe-5"></div>

                </div>
                <div className="col-md-6 order-first order-md-last mb-5 mb-md-0">
                    <img className="shadow-16dpi" src={acfAboutPageWhyChooseUs?.whyChooseUsImage?.sourceUrl} alt={acfAboutPageWhyChooseUs?.whyChooseUsImage?.altText} width="100%" />
                    <div className="shadow-16dpi">
                        {/* <Image
                            src={acfAboutPageWhyChooseUs?.whyChooseUsImage?.sourceUrl}
                            // just put the original width and height of the original image, in order to provide the right aspect ratio
                            // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                            width={760}
                            height={800}
                            // layout="fill"
                            // objectFit="contain"
                            alt={acfAboutPageWhyChooseUs?.whyChooseUsImage?.altText} /> */}
                    </div>
                </div>
            </div>

        </div>
    )
}

export default AboutpageWhyChooseUs;
