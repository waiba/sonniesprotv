import styles from './css/OurMission.module.css'

const OurMission = ({acfOurMisson})  => {
    return (
        <div className={`${styles.OurMissonSection} container-fluid ps-0 pe-0 px-md-5 px-4 px-lg-0`}>
            <div className="container-lg container-fluid text-center">
                <div className="col-md-12 col-xl-8 col-lg-10 m-auto">
                    <h3 className={`${styles.ourmissionHeading} pb-lg-4 pb-md-3 pb-3`}>{acfOurMisson?.missionTitle}</h3>
                    <p className={styles.ourmissionContent}>{acfOurMisson?.missionContent}</p>
                </div>
            </div>
        </div>
    )
}

export default OurMission;
