import Image from 'next/image';
import styles from './css/WhoWeAre.module.css'

const WhoWeAre = ({ acfWhoWeAre }) => {
    return (
        <div className={`${styles.whoWeAreSection} container-lg container-fluid px-md-5  px-lg-0 px-4`}>
            <div className="col-xl-8 col-lg-10 col-md-12 text-md-center text-start m-auto pb-md-4 ">
                <p className="smallSubTitle">{acfWhoWeAre?.smallTitleText}</p>
                <h3 className="sectionTitle mb-md-3 mb-lg-4 mb-3 ">{acfWhoWeAre?.mainTitleText}</h3>
                <p className="sectionMainContent pb-5 pb-md-0 ">{acfWhoWeAre?.mainContent}</p>
            </div>
            <div className="row pt-md-4">
                {
                    acfWhoWeAre?.singleBlocksWrap.map((value, key) => {
                        return (
                            <div className="col-lg-3 col-md-6 pb-md-4 pb-4 pb-lg-0" key={key}>
                                {/* <img src={value?.singleIcon?.sourceUrl} alt={value?.singleIcon?.altText} className={styles.singleImage} /> */}
                                <div className={styles.singleImage}>
                                    <Image
                                        src={value?.singleIcon?.sourceUrl}
                                        // just put the original width and height of the original image, in order to provide the right aspect ratio
                                        // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                        width={73}
                                        height={72}
                                        // layout="fill"
                                        // objectFit="contain"
                                        alt={value?.singleIcon?.altText} />
                                </div>
                                <h5 className={`${styles.singleTitle} pb-4`}>{value?.singleTitle}</h5>

                                <p className={`${styles.singleContent} pe-5 `}>{value?.singleContent}</p>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default WhoWeAre;
