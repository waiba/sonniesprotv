const YoutubeIframe = () => {
    return (
        <iframe src="https://www.youtube.com/embed/TWReOjguJ40?controls=0" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    )
}
export default YoutubeIframe;