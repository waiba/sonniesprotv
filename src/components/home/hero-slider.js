import { useEffect } from "react"
import Image from "next/image";
import { isEmpty } from "lodash";
//import Styles from './css/Testimonial.module.css'
import "../../../node_modules/slick-carousel/slick/slick.css";
//slick-carousel/slick/slick.css
//import "~slick-carousel/slick/slick-theme.css";
import "../../../node_modules/slick-carousel/slick/slick-theme.css";

import Slider from 'react-slick'

const HeroSlider = ({ HeroSlider }) => {

    const settings = {
        autoplay: true,
        autoplaySpeed: 4000,
        dots: false,
        infinite: true,
        className: "center",
        //centerMode: true,
        //infinite: true,
        //centerPadding: "50px",
        slidesToShow: 1,
        speed: 500,
        rows: 1,
        slidesPerRow: 1,
        arrows: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: false,
                }
            }
        ]
    };

    useEffect(() => {
        const elems = document.querySelectorAll(".slick-slide");

        var index = 0, length = elems.length;
        for (; index < length; index++) {
            const elementss = elems[index].childNodes;
            var i = 0, len = elementss.length;
            for (; i < len; i++) {
                elementss[i].className = "d-flex ";
            }
        }
        // document.querySelectorAll(".slick-slide").className =
        //   "testing";
    }, []);

    const testimonialPlatform = (value) => {
        if (value == 'Google') {
            return (
                <Image src="./google.png" width={53} height={53} />
            )
        }
        else {
            return (
                <Image src="/fb-icon.png" width={53} height={53} />
            )
        }
    }

    const testimonialStar = (value) => {
        var i = 1;
        var img = "";
        while (i <= value) {
            img += '<img src="./star.png" class="displayInline" />';
            i++;
        }
        return img;
    }


    return (

        <div className={`heroSlider container-fluid px-0 px-md-0 px-lg-0 HeroSliderSecitionWrap1 w-100`}>
            <Slider {...settings}>
                {
                    HeroSlider.map((value, key) => {

                        return (

                            <div className="align-self-stretch heroSliderImage1 homePageHeroBanner position-relative" data-attr={key} key={key} >
                                {!isEmpty(value?.slideGroup?.image?.sourceUrl) &&
                                    <Image
                                        src={value?.slideGroup?.image?.sourceUrl}
                                        // just put the original width and height of the original image, in order to provide the right aspect ratio
                                        // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                        width={1900}
                                        height={1000}
                                        // layout="fill"
                                        objectFit="cover"
                                        alt={value?.slideGroup?.image?.altText} />
                                }
                                {/* <div className="col-md-12"> */}

                                {/* </div> */}

                                <div className="container-fluid text-left position-absolute mt-auto mb-auto px-md-5 px-lg-0 px-md-4 px-0 heroSlideContent d-flex1 align-items-center1">
                                    <div className="col-xl-12 offset-xl-111 col-lg-12 col-md-12 px-lg-12 col-12 position-absolute bottomPosition pb-md-4 pb-3">
                                        <div className="container mx-auto mobileCenter" dangerouslySetInnerHTML={{ __html: value?.slideGroup?.content }} >
                                        </div>

                                    </div>
                                </div>
                            </div>
                        );
                    })
                }
            </Slider>
        </div>
    )
}
export default HeroSlider;