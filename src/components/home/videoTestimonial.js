import React, { useEffect, useState, useRef } from "react";
import Link from 'next/link'
import styles from './css/VideoTestimonial.module.css'
import useOnScreen from "../../utils/useOnScreen";
// import isEmpty from 'lodash'
// import Image from 'next/image'

import dynamic from 'next/dynamic'
//import YoutubeIframe from '../home/youtubeIframe';
const YoutubeIframe = dynamic(() => import('./youtubeIframe'), {
    ssr: false
});

const VideoTestimonial = ({ VideoTestimonial, children }) => {

    const [ready, setReady] = useState(false);

    // useEffect(() => {
    //     showIframe(); //fetch data on first load
    // }, [])

    function showIframe() {
        setReady(true);
        //console.log(ready);
    }


    const youtubeIframeRef = useRef();
    const youtubeIframeRefValue = useOnScreen(youtubeIframeRef);
    const [isyoutubeIframeRef, setyoutubeIframeRef] = useState(false);
    useEffect(() => {
        if (!isyoutubeIframeRef)
            setyoutubeIframeRef(youtubeIframeRefValue);
    }, [youtubeIframeRefValue])

    return (
        <div className={`container-fluid ${styles.testimonialBackground}`}>
            <div className={`${styles.VideoImageWrapper}`} >
                {/* <img src="./video-testimonial.jpg" width="100%"/> */}

                <div className={`${styles.videoTestimonialContentWrapper} container-fluid gx-0`} >
                    <div className="row">
                        <div className={`col-xxl-5 offset-xxl-1 col-lg-5 offset-lg-0 px-md-5 order-last order-lg-first ${styles.sectionPadding}`} >
                            <p className={`${styles.smallSubTitleBefore} smallSubTitle`}>{VideoTestimonial?.videoSmallTitleText}</p>
                            <h3 className="sectionTitle pb-md-4 pb-3">{VideoTestimonial?.videoMainTitleText}</h3>
                            <div dangerouslySetInnerHTML={{ __html: VideoTestimonial?.videoMainContent }} className={`sectionMainContent pe-5 pb-4`}>
                            </div>
                            {
                                (VideoTestimonial?.videoContentLink !== '') &&
                                (
                                    <Link href={VideoTestimonial?.videoContentLink}>
                                        <a className="btn primaryButton" target="_blank">{VideoTestimonial?.videoContentUrlText}</a>
                                    </Link>
                                )
                            }
                        </div>
                        <div className={`col-xxl-6 col-lg-7 order-first order-lg-last ${styles.backgroundTVImage} ${styles.iframePadding}`}>

                            {/* <Image src='/video-testimonial.jpg'
                                layout="fill"
                                objectFit="cover"
                            /> */}

                            <div className="ratio ratio-16x9" ref={youtubeIframeRef}>
                                {isyoutubeIframeRef &&
                                    <YoutubeIframe />
                                }


                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    )
}

export default VideoTestimonial;
