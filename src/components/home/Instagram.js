import { useEffect, useState } from 'react';
import Image from 'next/image'
import {isEmpty} from 'lodash';
import Slider from 'react-slick'
import fetch from 'isomorphic-unfetch';

import styles from './css/Instagram.module.css';
import Link from 'next/link';

const InstagramFeed = ({InstagramFeed}) =>  {


  const [values, setValues] = useState({
    data: false
});

const { data } = values;
  

  //console.log(res);
  //const { data } = res?.data;
  //const message  = res?.message;

  const instagramLoad = async (e) => {
    const response = await fetch('/api/instagram', {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'GET'
    }).then(response => response.json() );
    //return res.json({message: response});

    const message  = response?.message;

    if (!isEmpty(response?.message)) {
      setValues( { ...values, data: response?.message } )
      return;
    }
  }


    useEffect(() => {
      instagramLoad();  
    }, []);


    const settings = {
        dots: false,
        infinite: true,
        className: "instagramSlider",
        //centerMode: true,
        //infinite: true,
        //centerPadding: "50px",
        slidesToShow: 1,
        speed: 500,
        rows: 1,
        slidesPerRow: 5,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesPerRow: 4,
                infinite: true,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                slidesPerRow: 3,
                infinite: true,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesPerRow: 1,
                arrows: false,
              }
            }
          ]
      };

      const heroBannerstyling = {
        objectFit: 'cover'
    }

    return (
        <section className={`${styles.instragramFeedSection}`}>
        <Slider {...settings}>
          {
            data !== false && 
            data?.data?.map((value, key) => {
              return (
                <div className="col-md-2 text-center carouselHeightCss" key={key}>
                  
                  
                    {
                      (value?.media_type == 'IMAGE' || value?.media_type =='CAROUSEL_ALBUM') && 
                      <Link href={value?.permalink}><a target="_blank"><img src={value?.media_url} alt={value?.caption_text} className={`d-inline-block`} style={heroBannerstyling} /></a></Link>
                      //<Image src={value?.media_url} alt={value?.caption_text} className={`d-inline-block `} layout="responsive" sizes="(max-width: 600px) 200px, 50vw" style={heroBannerstyling}/> 
                    }
                    {
                      value?.media_type == 'VIDEO' &&
                      <Link href={value?.permalink}><a target="_blank"><img src={value?.thumbnail_url} alt={value?.caption_text} className={`d-inline-block`} style={heroBannerstyling} /></a></Link>
                      //<Image src={value?.thumbnail_url} alt={value?.caption_text} className={`d-inline-block `} width={320} height={320}  /> 
                    }
                    
                </div>
              )
            })
          }
        </Slider>
        </section>
    )
}


export default InstagramFeed;
