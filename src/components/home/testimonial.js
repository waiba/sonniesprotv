import { useEffect } from "react"
import Image from "next/image";
import { isEmpty } from "lodash";
import Styles from './css/Testimonial.module.css'
import "../../../node_modules/slick-carousel/slick/slick.css";
//slick-carousel/slick/slick.css
//import "~slick-carousel/slick/slick-theme.css";
import "../../../node_modules/slick-carousel/slick/slick-theme.css";

import Slider from 'react-slick'

const Testimonial = ({ Testimonials }) => {

    const settings = {
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        infinite: true,
        className: "center",
        //centerMode: true,
        //infinite: true,
        //centerPadding: "50px",
        slidesToShow: 1,
        speed: 500,
        rows: 2,
        slidesPerRow: 2,
        arrows: false,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    speed: 500,
                    rows: 1,
                    slidesPerRow: 1,
                    infinite: true,
                    dots: false,
                    adaptiveHeight: true,
                    arrows: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    speed: 500,
                    rows: 1,
                    slidesPerRow: 1,
                    infinite: true,
                    dots: false,
                    adaptiveHeight: true,
                    arrows: false,
                }
            }
        ]
    };

    useEffect(() => {
        const elems = document.querySelectorAll(".slick-slide");

        var index = 0, length = elems.length;
        for (; index < length; index++) {
            const elementss = elems[index].childNodes;
            var i = 0, len = elementss.length;
            for (; i < len; i++) {
                elementss[i].className = "d-flex ";
            }
        }
        // document.querySelectorAll(".slick-slide").className =
        //   "testing";
    }, []);

    const testimonialPlatform = (value) => {
        if (value == 'Google') {
            return (
                <Image src="/google.png" width={53} height={53} />
            )
        }
        else {
            return (
                <Image src="/fb-icon.png" width={53} height={53} />
            )
        }
    }

    const testimonialStar = (value) => {
        var i = 1;
        var img = "";
        while (i <= value) {
            img += '<img src="./star.png" class="displayInline" />';
            i++;
        }
        return img;
    }


    return (
        <div className={`${Styles.testimonialSection} ${Styles.testimonialPadding}`}>
            <div className={`container-lg container-fluid px-md-5 px-lg-0`}>
                <div className={`col-md-12 text-center pb-md-5 pb-3 ${Styles.sectionHeadingPaddingbottom}`}>
                    <p className="smallSubTitle">Testimonials</p>
                    <h3 className="sectionTitle">What our Customer say</h3>
                </div>
                <div className="testimonialsBlockWrap row">
                    <Slider {...settings}>
                        {
                            Testimonials.map((value, key) => {

                                return (

                                    <div className=" col-md-6 p-3 align-self-stretch" data-attr={key} key={key} >
                                        {/* <div className="col-md-12"> */}
                                        <div className={`customShadow  p-md-5 p-4 shadowTestimonial h-100`}>
                                            <div className="row">
                                                {!isEmpty(value?.node?.featuredImage?.node?.sourceUrl) &&
                                                    <div className="col-md-2 col-12 text-center text-md-start mb-5 mb-md-0">
                                                        {/* <img src={value?.node?.featuredImage?.node?.sourceUrl} alt={value?.node?.featuredImage?.node?.altText} className={`d-inline-block ${Styles.roundedImage}`} /> */}
                                                        <div className={`d-inline-block ${Styles.roundedImage}`}>
                                                            {/* {!isEmpty(value?.node?.featuredImage?.node?.sourceUrl) && */}
                                                            < Image
                                                                src={value?.node?.featuredImage?.node?.sourceUrl}
                                                                // just put the original width and height of the original image, in order to provide the right aspect ratio
                                                                // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                                                width={150}
                                                                height={150}
                                                                // layout="fill"
                                                                // objectFit="contain"
                                                                alt={value?.node?.featuredImage?.node?.altText} />
                                                            {/* } */}
                                                        </div>
                                                    </div>
                                                }
                                                <div className={isEmpty(value?.node?.featuredImage?.node?.sourceUrl) ? 'col-md-12 col-12' : 'col-md-10 col-12'} >
                                                    <div dangerouslySetInnerHTML={{ __html: value?.node?.content }} className={`text-center text-md-start ${Styles.singleTestimonialWrapContent}`}>
                                                    </div>
                                                    <div className={`col-md-12 text-center text-md-start ${Styles.starTestimonial}`} dangerouslySetInnerHTML={{ __html: testimonialStar(value?.node?.acfTestimonialInfo?.starRating) }}>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-10 col-12 text-center text-md-start">
                                                            <h4 className={Styles.testimonialName}>
                                                                {value?.node?.title}
                                                            </h4>
                                                            <p className={Styles.tetimonialLocation}>
                                                                {value?.node?.acfTestimonialInfo?.location}
                                                            </p>
                                                        </div>
                                                        <div className="col-md-2 text-center text-md-end col-12 ">
                                                            {testimonialPlatform(value?.node?.acfTestimonialInfo?.reviewPlatform)}
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        {/* </div> */}
                                    </div>
                                );
                            })
                        }
                    </Slider>
                </div>
            </div>
        </div>
    )





    // return (
    //     <div className={`testimonialSection container ${Styles.testimonialPadding}`}>
    //         <div className={`col-md-12 text-center ${Styles.sectionHeadingPaddingbottom}`}>
    //             <p className="smallSubTitle">Testimonials</p>
    //             <h3 className="sectionTitle">What our Customer say</h3>
    //         </div>
    //         <div className="testimonialsBlockWrap row">
    //             {
    //                 Testimonials.map((value,key) => {
    //                     console.log(value?.node?.acfTestimonialInfo?.reviewPlatform);


    //                     return (
    //                         <div className="col-md-6 mt-md-1 mb-md-4 pe-md-4" key={key} >
    //                             {/* <div className="col-md-12"> */}
    //                                 <div className={`shadow px-md-5 py-md-5 shadowTestimonial`}>
    //                                     <div className="row">
    //                                         <div className="col-md-2">
    //                                             <img src={value?.node?.featuredImage?.node?.sourceUrl} alt={value?.node?.featuredImage?.node?.altText} className={`${Styles.roundedImage}`} />
    //                                         </div>
    //                                         <div className="col-md-10">
    //                                             <div dangerouslySetInnerHTML={{__html: value?.node?.content}} className={`${Styles.singleTestimonialWrapContent}`}>
    //                                             </div>
    //                                             <div className={`col-md-12 ${Styles.starTestimonial}`} dangerouslySetInnerHTML={{__html: testimonialStar(value?.node?.acfTestimonialInfo?.starRating)}}>
    //                                             </div>
    //                                             <div className="row">
    //                                                 <div className="col-md-10">
    //                                                     <h4 className={Styles.testimonialName}>
    //                                                     {value?.node?.title}
    //                                                     </h4>
    //                                                     <p className={Styles.tetimonialLocation}>
    //                                                         {value?.node?.acfTestimonialInfo?.location}
    //                                                     </p>
    //                                                 </div>
    //                                                 <div className="col-md-2 text-md-end">
    //                                                     {testimonialPlatform(value?.node?.acfTestimonialInfo?.reviewPlatform)}
    //                                                 </div>
    //                                             </div>

    //                                         </div>
    //                                     </div>
    //                                 </div>
    //                             {/* </div> */}
    //                         </div>
    //                     );
    //                 })
    //             }
    //         </div>
    //     </div>
    // )
}

export default Testimonial;
