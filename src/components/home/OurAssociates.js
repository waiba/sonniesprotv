import Image from 'next/image'
import styles from './css/OurAssociates.module.css'
import Slider from 'react-slick'

const OurAssociates = ({ OurAssociates }) => {
  const settings = {
    dots: false,
    infinite: true,
    //className: "center",
    //centerMode: true,
    //infinite: true,
    //centerPadding: "50px",
    slidesToShow: 1,
    speed: 500,
    rows: 1,
    arrows: false,
    slidesPerRow: 5,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesPerRow: 4,
          infinite: true,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesPerRow: 3,
          infinite: true,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesPerRow: 2,
          arrows: false,
        }
      }
    ]
  };

  return (
    <div className={`associateContainer container-lg container-fluid px-4 px-md-0${styles.sectionPadding}`}>
      <div className="col-md-12 text-center">
        <h3 className="sectionTitle">Our Associates</h3>
      </div>
      <div className={`${styles.OurAssociatesSlider} col-md-12 pt-md-5 pt-3`}>
        <Slider {...settings}>
          {
            OurAssociates.map((value, key) => {
              return (
                <div className="col-md-2 text-center" key={key}>
                  {/* <img src={value?.node?.featuredImage?.node?.sourceUrl} alt={value?.node?.featuredImage?.node?.altText} /> */}
                  <Image
                    src={value?.node?.featuredImage?.node?.sourceUrl}
                    // just put the original width and height of the original image, in order to provide the right aspect ratio
                    // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                    width={219}
                    height={97}
                    alt={value?.node?.featuredImage?.node?.altText} />
                </div>
              )
            })
          }
        </Slider>
      </div>
    </div>
  )
}

export default OurAssociates;
