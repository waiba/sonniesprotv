import Link from 'next/link';
import Image from 'next/image'
import { isEmpty } from 'lodash'
import styles from './css/Services.module.css'


const Services = ({ Services }) => {

    const isEven = (n) => {
        return n % 2 == 0;
    }

    if (isEmpty({ Services })) {
        return false;
    }

    return (
        <div className={`servicesSection container-lg container-fluid px-md-5 px-4 px-lg-0 ${styles.secondarySectionPadding}`}>
            <div className={`col-md-12 text-center   pb-md-3 pb-3 ${styles.sectionHeadingPaddingbottom}`}>
                <p className="smallSubTitle">{Services?.serviceSmallTitleText}</p>
                <h3 className="sectionTitle">{Services?.serviceMainTitleText}</h3>
            </div>
            <div className="serviceBlockWrap">
                {
                    Services?.serviceSingleBlock.map((value, key) => {
                        if (isEven(key) === true) {
                            return (
                                <div className={`row py-lg-5 py-md-3 ${styles.singleServiceWrap} singleServiceBlockWrapper`} key={key}>
                                    <div className="col-xxl-4 offset-xxl-1 col-xl-5 col-lg-5 col-md-5 pe-lg-5 pe-md-2 mt-lg-5 mt-md-0 order-last order-md-first">
                                        <h4 className={`pb-lg-4 ${styles.servicesSingleTitle}`}>{value?.serviceTitle}</h4>
                                        <div dangerouslySetInnerHTML={{ __html: value?.serviceContent }} className="singleServiceWrapContent pe-lg-5 pe-md-0">

                                        </div>
                                        <Link href={value?.serviceLink?.url} key={key}>
                                            <a className="btn textButton">{value?.linkText}</a>
                                        </Link>
                                    </div>
                                    <div className="col-md-7 order-first order-md-last mb-5 mb-md-0">
                                        {/* <img alt={value?.serviceImage?.altText} src={value?.serviceImage?.sourceUrl} /> */}
                                        <Image
                                            src={value?.serviceImage?.sourceUrl}
                                            // just put the original width and height of the original image, in order to provide the right aspect ratio
                                            // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                            width={747}
                                            height={560}
                                            layout="responsive"
                                            placeholder="blur-up"
                                            alt={value?.serviceImage?.altText} />
                                    </div>
                                </div>
                            )
                        }
                        else {
                            return (
                                <div className={`row py-5 ${styles.singleServiceWrap} singleServiceBlockWrapper`} key={key}>
                                    <div className="col-xxl-7 col-xl-7 col-lg-7 col-md-7 mb-5 mb-md-0 ">
                                        <Image
                                            src={value?.serviceImage?.sourceUrl}
                                            // just put the original width and height of the original image, in order to provide the right aspect ratio
                                            // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                            width={747}
                                            height={560}
                                            layout="responsive"
                                            placeholder="blur"
                                            alt={value?.serviceImage?.altText} />
                                    </div>


                                    <div className="col-md-5   ps-md-5 mt-lg-5 mt-md-0">


                                        <h4 className={`pb-lg-4 ${styles.servicesSingleTitle}`}>{value?.serviceTitle}</h4>
                                        <div dangerouslySetInnerHTML={{ __html: value?.serviceContent }} className="singleServiceWrapContent   pe-xl-5 pe-lg-0 pe-md-0">

                                        </div>
                                        <Link href={value?.serviceLink?.url} key={key}>
                                            <a className="btn textButton">{value?.linkText}</a>
                                        </Link>
                                    </div>
                                </div>
                            )
                        }
                    })
                }
            </div>
            <div className="col-md-12 text-center pt-5">
                <Link href="/services/">
                    <a className="btn primaryButton ">View All Services</a>
                </Link>
            </div>
        </div>
    )
}

export default Services;
