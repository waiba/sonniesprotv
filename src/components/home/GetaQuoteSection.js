//import Link from 'next/link'
import Image from 'next/image';
import styles from './css/GetaQuoteSection.module.css'
import { isEmpty } from 'lodash'
import ModalExample from '../layout/header/getaQuoteModal'

const GetaQuote = ({ acfGetaQuote, pageSlug }) => {
    return (
        <div className="container-fluid ps-0 pe-0">
            <div className={`${styles.VideoImageWrapper} getaQuoteSection`} >
                <div className="getaQuotebackgroundImage">
                    <Image
                        src={acfGetaQuote?.getAQuoteBackgroundImage?.sourceUrl}
                        // just put the original width and height of the original image, in order to provide the right aspect ratio
                        // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                        layout="fill"
                        objectFit="cover"
                        alt={acfGetaQuote?.getAQuoteBackgroundImage?.altText} />
                </div>


                <div className={`${styles.getAQuoteContentWrapper} getaQuoteContentWrapper container-lg container-fluid px-md-5 px-lg-0 px-md-4 px-sm-4 px-3 sectionPadding`} >
                    <div className="col-md-12">
                        <div className="col-xxl-7 offset-xxl-1 col-xl-8 offset-xxl-1  col-lg-9 col-md-9 ">
                            <h3 className={`sectionTitle pb-md-4 pb-sm-4 pb-2 ${styles.getAQuoteTitle}`}>{acfGetaQuote?.getAQuoteTitle}</h3>
                            {!isEmpty(acfGetaQuote?.getAQuoteContent) &&
                                <div className={`sectionMainContent ${styles.getAQuoteContent}`}>
                                    <p className="pe-sm-5 pe-md-5 pe-0 pb-md-4 pb-sm-4 pb-2">{acfGetaQuote?.getAQuoteContent}</p>
                                </div>
                            }
                            {
                                // (pageSlug === true) ?
                                // (
                                <ModalExample info={acfGetaQuote?.getAQuoteLinkText} buttonPosition={'getaQuoteSection'} />
                                // ) : (
                                //     <Link href="/services">
                                //         <a className={`btn primaryButton  ${styles.blueColor}`}>{acfGetaQuote?.getAQuoteLinkText}</a>
                                //     </Link>
                                // )
                            }

                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default GetaQuote;
