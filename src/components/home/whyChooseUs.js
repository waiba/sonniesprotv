import Link from 'next/link';
import { isEmpty } from 'lodash'
import styles from './WhyChooseUs.module.css'

const WhyChooseUs = ({ WhyChooseUs }) => {
    //const singleWrap = (i, k)
    if (isEmpty({ WhyChooseUs })) {
        return false;
    }

    return (
        <div className="whyChooseUsSection container-lg container-fluid sectionPadding px-md-5  px-lg-0 px-4">
            <div className="row">
                <div className="col-xxl-5 offset-xxl-1 col-xl-6 col-lg-5 col-md-12 mb-md-5 mb-lg-0 mb-5">
                    <p className="smallSubTitle">{WhyChooseUs?.smallTitleText}</p>
                    <h3 className="sectionTitle pb-lg-5 pb-md-3 pb-3">{WhyChooseUs?.mainTitleText}</h3>
                    <p className="sectionMainContent  pb-lg-5 pb-md-3 pe-lg-5 pe-md-0 pb-3">{WhyChooseUs?.mainContent}</p>
                    <Link href={WhyChooseUs?.mainContentLink?.url}>
                        <a className="btn primaryButton">{WhyChooseUs?.mainContentUrlText}</a>
                    </Link>
                </div>
                <div className="col-xxl-6 col-lg-7 col-xl-6 col-md-12">
                    <div className="row">
                        {
                            WhyChooseUs?.singleBlocksWrap.map((value, key) => {
                                return (
                                    <div className="col-md-6 col-lg-6  pe-lg-5 pe-md-5 mb-lg-5 mb-md-4 mb-4 " key={key}>
                                        <img src={value?.singleIcon?.sourceUrl} alt={value?.singleIcon?.altText} className={styles.singleImage} />
                                        <h5 className={styles.singleTitle}>{value?.singleTitle}</h5>
                                        <p className={styles.singleContent}>{value?.singleContent}</p>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WhyChooseUs;
