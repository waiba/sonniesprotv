import CountUp from "react-countup"
import VisibilitySensor from "react-visibility-sensor";
import Styles from './css/Counter.module.css'
const Counter = ({ Counter }) => {
    return (
        <div className={`counterSection container-lg container-fluid  ${Styles.counterPadding}`}>
            <h4 className={`text-center pb-md-4 pb-3 px-4 px-md-0 ${Styles.sectionTitle}`}>{Counter?.counterTitle}</h4>
            <div className="col-md-10 offset-md-1">
                <div className="row">
                    {
                        Counter?.singleCounterBlock.map((value, key) => {
                            var suffixText = '';
                            if (value?.title == 'Installation') {
                                var suffixText = 'K';
                            }
                            if (value?.title == 'Years in Service') {
                                var suffixText = '+';
                            }
                            return (
                                <VisibilitySensor>
                                    {({ isVisible }) => (
                                        <div className="col-md-3 text-center" key={key}>

                                            <h5 className={Styles.heading5}>
                                                {isVisible ? <CountUp start={0} suffix={suffixText} end={value?.totalNumber} duration={3} redraw={false} /> : null}
                                            </h5>


                                            <p className={Styles.titleStyle}>{value?.title}</p>
                                        </div>
                                    )}
                                </VisibilitySensor>
                            )
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default Counter;
