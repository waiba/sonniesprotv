import {isEmpty} from 'lodash'
import styles from './css/ServicesIntro.module.css'

const ServicesIntro = ({acfServicesIntroSection}) => {
    if(isEmpty(acfServicesIntroSection) || (isEmpty(acfServicesIntroSection?.servicesIntroTitle) && isEmpty(acfServicesIntroSection?.servicesIntroContent) )) {
        return null;
    }
    return (
        <div className={`${styles.servicesIntroSection} container-lg container-fluid px-md-5 px-4 px-lg-0`}>
            <div className="col-md-12 col-lg-10 col-xl-8 m-auto text-center">
                <h3 className="sectionTitle pb-md-4">{acfServicesIntroSection?.servicesIntroTitle}</h3>
                <p>{acfServicesIntroSection?.servicesIntroContent}</p>
            </div>
        </div>
    )
}

export default ServicesIntro;