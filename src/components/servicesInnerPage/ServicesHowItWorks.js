import { isEmpty } from 'lodash'
import styles from './css/ServicesHowItWorks.module.css'

const ServicesHowItWorks = ({ acfServicesProcessSection }) => {
    if (isEmpty(acfServicesProcessSection)) {
        return;
    }

    const isEven = (n) => {
        return n % 2 == 0;
    }


    return (
        <div className={`ServicesHowItWorksSection ${styles.ServicesHowItWorksSection1} container-fluid px-md-5 px-4 px-lg-0`}>
            <div className="col-md-12 col-lg-10 col-xl-8 m-auto text-center">
                <p className="smallSubTitle">{acfServicesProcessSection?.serviceProcessSmallTitle}</p>
                <h3 className="sectionTitle">{acfServicesProcessSection?.serviceProcessTitle}</h3>
            </div>
            <div className="container-lg container-fluid ">
                <div className="row gx-0">
                    <div className="col-md-12 col-lg-4 mainContent pt-lg-5 pt-4 pe-md-5" dangerouslySetInnerHTML={{ __html: acfServicesProcessSection?.serviceFeatureContent }}>

                    </div>
                    <div className="col-md-12 col-lg-8 pt-md-4 pt-2">
                        <div className={`row d-flex flex-row-reverse1 ${styles.countSet}`}>
                            {
                                acfServicesProcessSection?.serviceProcessBlock &&

                                acfServicesProcessSection?.serviceProcessBlock.map((value, key) => {
                                    if (isEven(key) === true) {
                                        return (
                                            <div className={`col-md-6 col-12 pe-md-5 pe-2 ps-2 ps-md-0  mb-4 mb-md-0 styles.dottedBerticalBorder align-self-center ${styles.oddNumber} ${styles.countNumber} ${key}`} key={key}>
                                                <div className={`p-4 customShadow border ${styles.singleBlockContent} ${styles.oddNumberHorizontalLine}`}>
                                                    <div className="row">
                                                        <div className="col-md-2">
                                                            <img src="/checked.png" className="mb-3 mb-md-0" />
                                                        </div>
                                                        <div className="col-md-10">
                                                            <p dangerouslySetInnerHTML={{ __html: value?.serviceProcessSingle }}></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    } else {
                                        return (
                                            <div className={`col-md-6 col-12 ps-md-5 ps-2 pe-2 pe-md-0 mb-4 mb-md-0 ${styles.countNumber} ${styles.evenNumber} ${key}`} key={key}>
                                                <div className={`mt-md-4 mb-md-2 p-4 customShadow border ${styles.singleBlockContent} ${styles.evenNumberHorizontalLine}`}>
                                                    <div className="row">
                                                        <div className="col-md-2">
                                                            <img src="/checked.png" className="mb-3 mb-md-0" />
                                                        </div>
                                                        <div className="col-md-10">
                                                            <p dangerouslySetInnerHTML={{ __html: value?.serviceProcessSingle }}></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    }
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ServicesHowItWorks;
