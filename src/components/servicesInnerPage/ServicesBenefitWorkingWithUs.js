import Image from 'next/image'
import { isEmpty } from 'lodash'
import styles from './css/ServicesBenefitWorkingWithUs.module.css'

const ServicesBenefitWorkingWithUs = ({ acfServicesBenefitWorkingWithUs }) => {

    if (isEmpty(acfServicesBenefitWorkingWithUs)) {
        return;
    }

    return (
        <div className={`container-fluid ${styles.servicesBg}`}>
            <div className={`${styles.ourCoreValuesSection} container-lg container-fluid px-md-5 px-4 px-lg-0`}>
                <div className="col-md-12 col-lg-10 col-xl-8 m-auto text-center">
                    <h3 className="sectionTitle pb-4">{acfServicesBenefitWorkingWithUs?.servicesBenefitWorkingWithUsTitle}</h3>
                    <p className="sectionMainContent mb-5">{acfServicesBenefitWorkingWithUs?.servicesBenefitWorkingWithUsContent}</p>
                </div>
                <div className="row">
                    {
                        acfServicesBenefitWorkingWithUs?.servicesBenefitWorkingWithUsBlock &&
                        acfServicesBenefitWorkingWithUs?.servicesBenefitWorkingWithUsBlock.map((value, key) => {
                            return (
                                <div className="col-md-4 text-center px-4 px-lg-5 pb-md-0 pb-3" key={key}>
                                    {/* <img src={value?.benefitSingleBlockImage?.sourceUrl} alt={value?.benefitSingleBlockImage?.altText} className={styles.singleImage} /> */}
                                    <div className={styles.singleImage}>
                                        <Image
                                            src={value?.benefitSingleBlockImage?.sourceUrl}
                                            // just put the original width and height of the original image, in order to provide the right aspect ratio
                                            // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                            width={72}
                                            height={72}
                                            // layout="fill"
                                            // objectFit="contain"
                                            alt={value?.benefitSingleBlockImage?.altText} />
                                    </div>
                                    <h5 className={`${styles.singleTitle} pb-4`}>{value?.benefitSingleBlockTitle}</h5>
                                    <p className={styles.singleContent}>{value?.benefitSingleBlockContent}</p>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </div>
    )
}
export default ServicesBenefitWorkingWithUs;