import {isEmpty} from 'lodash'
import styles from './css/ServicesFeatures.module.css'

const ServicesFeatures = ({acfServicesFeatureSection}) => {
    if(isEmpty(acfServicesFeatureSection)) {
        return;
    }
    return (
        <div className={`${styles.servicesFeaturesSection} container-lg container-fluid px-md-5 px-4 px-lg-0`}>
            <div className="col-md-12 col-lg-10 col-xl-8 m-auto text-center">
                <p className="smallSubTitle">{acfServicesFeatureSection?.servicesFeatureSmallTitle}</p>
                <h3 className="sectionTitle">{acfServicesFeatureSection?.servicesFeatureTitle}</h3>
            </div>
            <div className="col-md-12 col-xl-10 m-auto pt-md-5">
                <div className="row">
                { acfServicesFeatureSection?.serviceFeatureBlock &&
                    acfServicesFeatureSection?.serviceFeatureBlock.map((value, key) => {
                        return (
                            <div className="col-md-4 mb-md-4" key={key}>
                                <p className={`${styles.featuresListWrap} ps-5`}>{value?.featureSingleBlock}</p>
                            </div>
                        )
                    })
                }
                </div>
            </div>
        </div>
    )
}

export default ServicesFeatures;
