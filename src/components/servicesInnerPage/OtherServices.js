import {isEmpty} from 'lodash'
import styles from './css/ServicesFeatures.module.css'

const OtherServices = ({acfOtherServices}) => {
    if(isEmpty(acfOtherServices)) {
        return;
    }
    return (
        <div className={`${styles.servicesFeaturesSection} container-lg container-fluid px-md-5 px-4 px-lg-0`}>
            <div className="col-md-12 col-lg-10 col-xl-8 m-auto text-center">
                <h3 className="sectionTitle">{acfOtherServices?.otherServicesSectionTitle}</h3>
            </div>
            <div className="col-md-12 col-xl-10 m-auto pt-md-5">
                <div className="row" dangerouslySetInnerHTML={{__html:acfOtherServices?.otherServicesSectionContent}}>
                </div>
            </div>
        </div>
    )
}

export default OtherServices;
