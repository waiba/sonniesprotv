import Image from 'next/image'
import { isEmpty } from 'lodash'
import styles from './css/ServicesWhyWorkWithUs.module.css'

const ServicesWhyWorkWithUs = ({ acfServicesWhyWorkWithUs }) => {
    if (isEmpty(acfServicesWhyWorkWithUs)) {
        return;
    }
    return (
        <div className={`${styles.servicesWhyWorkWithUsSection} container-lg container-fluid px-md-5 px-4 px-lg-0`}>

            <div className="serviceBlockWrap">
                <div className={`row ${styles.singleServiceWrap} singleServiceBlockWrapper`} >
                    <div className="col-xxl-4 offset-xxl-1 col-xl-5 col-lg-5 col-md-5 pe-lg-5 pe-md-2 pt-lg-5 pt-md-0 order-last order-md-first">
                        <p className="smallSubTitle">{acfServicesWhyWorkWithUs?.whyWorkWithUsSmallTitle}</p>
                        <h3 className={`${styles.servicesSingleTitle} sectionTitle pb-md-5 pb-4`}>{acfServicesWhyWorkWithUs?.whyWorkWithUsTitle}</h3>
                        <div dangerouslySetInnerHTML={{ __html: acfServicesWhyWorkWithUs?.whyWorkWithUsContent }} className="singleServiceWrapContent">

                        </div>
                    </div>
                    <div className="col-md-7 order-first order-md-last mb-5 mb-md-0">
                        {!isEmpty(acfServicesWhyWorkWithUs?.whyWorkWithUsImage?.sourceUrl) &&
                            <Image
                                src={acfServicesWhyWorkWithUs?.whyWorkWithUsImage?.sourceUrl}
                                // just put the original width and height of the original image, in order to provide the right aspect ratio
                                // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                width={760}
                                height={597}
                                // layout="fill"
                                // objectFit="contain"
                                alt={acfServicesWhyWorkWithUs?.whyWorkWithUsImage?.altText} />
                        }

                    </div>
                </div>
            </div>
        </div>
    )
}

export default ServicesWhyWorkWithUs;