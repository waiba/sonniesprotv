import React, { lazy } from 'react'
//import dynamic from 'next/dynamic'
// const Header = lazy(() => import("./header"));

// const Footer = lazy(() => import("./footer"));
// const WhyChooseUs = lazy(() => import('../home/whyChooseUs'));
// const Services = lazy(() => import("../home/services"));
// const Counter = lazy(() => import("../home/counter"));
// const Testimonial = lazy(() => import("../home/testimonial"));
// const VideoTestimonial = lazy(() => import("../home/videoTestimonial"));
// const InstagramFeed = lazy(() => import("../home/Instagram"));
// const OurAssociates = lazy(() => import("../home/OurAssociates"));
// const GetaQuote = lazy(() => import("../home/GetaQuoteSection"));
// const Subscribe = lazy(() => import("./Subscribe"));
// const FaqLayout = lazy(() => import('./faq'));
// import { isEmpty } from 'lodash'
// const Seo = lazy(() => import('../seo'));
// import { sanitize } from "../../utils/miscellaneous";

// import Head from "next/head"
// const GalleryImages = lazy(() => import('./galleryImages'));


// const Header = dynamic(() => import("./header"));

// const Footer = dynamic(() => import("./footer"));
// const WhyChooseUs = dynamic(() => import('../home/whyChooseUs'));
// const Services = dynamic(() => import("../home/services"));
// const Counter = dynamic(() => import("../home/counter"));
// const Testimonial = dynamic(() => import("../home/testimonial"));
// const VideoTestimonial = dynamic(() => import("../home/videoTestimonial"));
// const InstagramFeed = dynamic(() => import("../home/Instagram"));
// const OurAssociates = dynamic(() => import("../home/OurAssociates"));
// const GetaQuote = dynamic(() => import("../home/GetaQuoteSection"));
// const Subscribe = dynamic(() => import("./Subscribe"));
// const FaqLayout = dynamic(() => import('./faq'));
// import { isEmpty } from 'lodash'
// const Seo = dynamic(() => import('../seo'));
// import { sanitize } from "../../utils/miscellaneous";

// import Head from "next/head"
// const GalleryImages = dynamic(() => import('./galleryImages'));



import Header from "./header";
import Footer from "./footer"
import WhyChooseUs from '../home/whyChooseUs'
import Services from "../home/services";
import Counter from "../home/counter";
import Testimonial from "../home/testimonial";
import VideoTestimonial from "../home/videoTestimonial";
// import InstagramFeed from "../home/Instagram";
import OurAssociates from "../home/OurAssociates";
import GetaQuote from "../home/GetaQuoteSection";
import Subscribe from "./Subscribe";
import FaqLayout from './faq';
import { isEmpty } from 'lodash'
import Seo from '../seo';
import { sanitize } from "../../utils/miscellaneous";
//import jquery from 'jquery'; 
import Head from "next/head"
import GalleryImages from './galleryImages';

const Layout = ({ data, children }) => {

    return (
        <div className={data?.page?.slug}>
            <Seo seo={data?.page?.seo} uri={data?.page?.uri} />
            <Head>
                <meta charSet="UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="shortcut icon" href="/favicon.ico" />
                {data?.page?.seo?.schemaDetails && (
                    <script
                        type='application/ld+json'
                        className='yoast-schema-graph'
                        key='yoastSchema'
                        dangerouslySetInnerHTML={{ __html: sanitize(data?.page?.seo?.schemaDetails) }}
                    />
                )}


                {/* <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" /> */}
                {/* <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossOrigin="anonymous" /> */}
                {/* <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.css" /> */}
                <link rel="preconnect" href="https://fonts.gstatic.com" />
                <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@400;600;700&family=Roboto:wght@400;500&display=swap" rel="stylesheet" />
                {/* <link rel='stylesheet' id='jr-insta-styles-css'  href='https://sonnies.sonniesprotv.com.au/wp-content/plugins/instagram-slider-widget/assets/css/jr-insta.css?ver=1.8.4' media='all' /> */}
                {/* <link rel='stylesheet' id='jr-insta-styles-css' href='https://sonnies.sonniesprotv.com.au/wp-includes/css/dist/block-library/style.min.css?ver=5.6.1' media='all' /> */}
                {/* <link rel='stylesheet' id='wis_font-awesome-css' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=5.6' media='all' /> */}
                {/* <link rel='stylesheet' id='wis_instag-slider-css'  href='http://sonniesprotv.redtravellers.com/wp-content/plugins/instagram-slider-widget/assets/css/instag-slider.css?ver=1.8.4' media='all' />
            <link rel='stylesheet' id='wis_wis-header-css'  href='http://sonniesprotv.redtravellers.com/wp-content/plugins/instagram-slider-widget/assets/css/wis-header.css?ver=1.8.4' media='all' />
            <script src='http://sonniesprotv.redtravellers.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1' id='jquery-core-js'></script>
            <script src='http://sonniesprotv.redtravellers.com/wp-content/plugins/instagram-slider-widget/assets/js/jquery.flexslider-min.js?ver=1.8.4' id='wis_jquery-pllexi-slider-js' async></script> */}
                <script async src="https://www.googletagmanager.com/gtag/js?id=G-V6Q6T84TRS"></script>
                <script dangerouslySetInnerHTML={{
                    __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'G-V6Q6T84TRS');`}}>
                </script>

            </Head>
            <Header header={data?.header} menus={data?.menus?.mainMenus} isFrontPage={data?.page?.isFrontPage} heroBannerInfo={data?.page?.acfHomePageBanner} heroSliderInfo={data?.page?.acfHeroSlider?.slides} seo={data?.page?.seo} uri={data?.page?.uri} page404={false} />
            {/* <div className="container">
                <img src="http://localhost/sonniesprotv/assets/landscape-3639879_1920.jpg" className="img-responsive" width="100%"/>
            </div> */}
            <div className="pageContent">
                {
                    !isEmpty(data?.page?.content) &&
                    <div dangerouslySetInnerHTML={{ __html: data?.page?.content }} className="container sectionPadding120">

                    </div>
                }

                {children}

                {
                    data?.page?.isFrontPage === true ? (
                        <React.Fragment>
                            <WhyChooseUs WhyChooseUs={data?.page?.acfHomePageWhyChooseUs} />
                            <Services Services={data?.page?.acfServices} />
                            <VideoTestimonial VideoTestimonial={data?.page?.acf2MinuteVideo} />
                            <Counter Counter={data?.page?.acfCounterBlock} />
                            <Testimonial Testimonials={data?.testimonials?.edges} />
                            {/* <InstagramFeed InstagramFeed={data?.page?.acfInstagramFeed} /> */}
                            <OurAssociates OurAssociates={data?.associates} />
                        </React.Fragment>
                    ) : ''
                }

                {
                    data?.page?.isFrontPage === true ? (
                        <React.Fragment>
                            <GetaQuote acfGetaQuote={data?.page?.acfGetaQuote} pageSlug={data?.page?.isFrontPage} />
                            <Subscribe acfContactInfo={data?.page?.acfContactInfo} />
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            {!isEmpty(data?.page?.acfFaqs?.faqSingle) &&
                                <FaqLayout faqs={data?.page?.acfFaqs?.faqSingle} />
                            }

                            {!isEmpty(data?.page?.acfGallery?.galleryImages) &&
                                <GalleryImages acfGallery={data?.page?.acfGallery} />
                            }
                            {
                                (data?.page?.slug != 'get-a-quote') &&
                                <GetaQuote acfGetaQuote={data?.homePage?.acfGetaQuote} />
                            }
                            {
                                (data?.page?.slug != 'get-a-quote') &&
                                <Subscribe acfContactInfo={data?.homePage?.acfContactInfo} />
                            }

                        </React.Fragment>
                    )
                }

            </div>

            <Footer footer={data?.footer} />
        </div>
    )
}

export default Layout;