import Header from "./header";
import Footer from "./footer"
import GetaQuote from "../home/GetaQuoteSection";
import Subscribe from "./Subscribe";
import ContactBody from "../contact/ContactBody";
//import jquery from 'jquery'; 

const ContactLayout = ({data, children}) => {
    //console.log(data?.page?.content);
    
    return(
        <div>
            <Header header={data?.header} menus={data?.menus?.mainMenus} isFrontPage={data?.page?.isFrontPage} heroBannerInfo={data?.page?.acfHomePageBanner} />

            {
                data?.homePage && 
                    <div className="pageContent">
                        <ContactBody contactForm={data?.page?.acfContactUsFields} homeContactInfo={data?.homePage?.acfContactInfo}/>
                        <GetaQuote acfGetaQuote={data?.page?.acfGetaQuote} />
                        <Subscribe acfContactInfo={data?.homePage?.acfContactInfo}/>
                    </div>
            }
            
            
            <div dangerouslySetInnerHTML={{__html: data?.page?.content}}>
                
            </div>
            {children}
            <Footer footer={data?.footer} />
        </div>
    )
}

export default ContactLayout;