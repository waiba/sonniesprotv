import Image from 'next/image'
import { isEmpty } from 'lodash'
import Link from 'next/link'
import styles from './index.module.css'
const ArchiveBlock = ({ archives }) => {
    return (
        <div className={`${styles.archivesBlockSection} container-lg container-fluid px-4 px-md-5 px-lg-0`}>
            <div className="row">
                {
                    archives.map((value, key) => {
                        return (
                            <div className="col-md-6 col-lg-4 mb-5" key={key}>
                                <div className="singleBlockWrap ps-4 pe-4 pt-4 pb-4 border">

                                    <div className={`${styles.singleImage} mb-3 shadow-16dpi`}>
                                        {!isEmpty(value?.node?.featuredImage?.node?.sourceUrl) &&
                                            <Image
                                                src={value?.node?.featuredImage?.node?.sourceUrl}
                                                // just put the original width and height of the original image, in order to provide the right aspect ratio
                                                // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                                width={374}
                                                height={259}
                                                // layout="fill"
                                                // objectFit="contain"
                                                alt={value?.node?.featuredImage?.node?.altText} />
                                        }

                                    </div>
                                    <h4 className={`${styles.singleBlockTitle} pt-4 pb-4`}>{value?.node?.title}</h4>
                                    <div dangerouslySetInnerHTML={{ __html: value?.node?.excerpt }} className={`${styles.singleBlockExcerpt} pb-4 `}>

                                    </div>
                                    <Link href={value?.node?.uri}>
                                        <a className="btn textButton ps-0 mb-3">Learn More</a>
                                    </Link>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default ArchiveBlock;
