import Header from "./header";
import Footer from "./footer"
import GetaQuote from "../home/GetaQuoteSection";
import Subscribe from "./Subscribe";
import ArchiveBlock from "./archives";
import Seo from "../seo";
import Head from "next/head";
import { sanitize } from "../../utils/miscellaneous";

const ArchivePages = ({data, children}) => {
    return (
        <div>
            <Seo seo={data?.page?.seo} uri={data?.page?.uri} />
            <Head>
            <link rel="shortcut icon" href={ data?.header?.favicon } />
				{data?.page?.seo?.schemaDetails && (
					<script
						type='application/ld+json'
						className='yoast-schema-graph'
						key='yoastSchema'
						dangerouslySetInnerHTML={{ __html: sanitize(data?.page?.seo?.schemaDetails) }}
					/>
				)}
            </Head>
            <Header header={data?.header} menus={data?.menus?.mainMenus} isFrontPage={data?.page?.isFrontPage} heroBannerInfo={data?.page?.acfHomePageBanner} seo={data?.page?.seo} uri={data?.page?.uri} page404={false} />
            {/* <div className="container">
                <img src="http://localhost/sonniesprotv/assets/landscape-3639879_1920.jpg" className="img-responsive" width="100%"/>
            </div> */}

            
            <div className="pageContent">
                <ArchiveBlock archives={data?.services?.edges}/>
                <GetaQuote acfGetaQuote={data?.page?.acfGetaQuote} />
                <Subscribe acfContactInfo={data?.homePage?.acfContactInfo}/>
            </div>            
            {children}
            <Footer footer={data?.footer} />
        </div>
    )
}

export default ArchivePages;