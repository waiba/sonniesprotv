//import { isNonEmptyArray } from "@apollo/client/utilities";

import Router from 'next/router';
import Image from 'next/image';
import Nprogress from 'nprogress';
import { isEmpty } from 'lodash'
import Link from 'next/link';
import HeroSlider from '../../home/hero-slider';

import MainMenu from "./nav"

Router.onRouteChangeStart = url => Nprogress.start();
Router.onRouteChangeComplete = url => Nprogress.done();
Router.onRouteChangeError = url => Nprogress.done();

const Header = ({ header, menus, isFrontPage, heroBannerInfo, heroSliderInfo, seo, uri, page404 }) => {

    if (isEmpty({ menus })) {
        return null;
    }

    const heroBannerstyling = {
        backgroundImage: (!isEmpty(heroBannerInfo?.heroBanner?.sourceUrl) ? `url('${heroBannerInfo?.heroBanner?.sourceUrl}')` : 'url(/services-banner.png)'),
        backgroundSize: 'cover'
    }

    const breadcrumbLength = seo?.breadcrumbs.length;

    const domainurlStrip = (url) => {
        /*
        * Replace base URL in given string, if it exists, and return the result.
        *
        * e.g. "http://localhost:8000/api/v1/blah/" becomes "/api/v1/blah/"
        *      "/api/v1/blah/" stays "/api/v1/blah/"
        */
        var baseUrlPattern = /^https?:\/\/[a-z\:0-9.]+/;
        var result = "";

        var match = baseUrlPattern.exec(url);
        if (match != null) {
            result = match[0];
        }

        if (result.length > 0) {
            url = url.replace(result, "");
        }

        return url;
    }






    return (

        <>
            <MainMenu header={header} menus={menus} isFrontPage={isFrontPage} />
            {
                isFrontPage === true && (

                    <HeroSlider HeroSlider={heroSliderInfo} />

                )
            }
            {
                isFrontPage !== true &&
                (
                    page404 === false &&
                    // <div style={heroBannerstyling} className="bannerWrapper">
                    <div className="bannerWrapper">
                        <div className="heroBannerImage">
                            <Image
                                src={(!isEmpty(heroBannerInfo?.heroBanner?.sourceUrl) ? heroBannerInfo?.heroBanner?.sourceUrl : '/services-banner.png')}
                                // just put the original width and height of the original image, in order to provide the right aspect ratio
                                // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                // width={1440}
                                // height={442}
                                layout="fill"
                                objectFit="cover"
                                alt={heroBannerInfo?.heroBanner?.altText} />
                        </div>
                        <div className="container breadCrumbs pt-5">
                            {

                                seo?.breadcrumbs.map((value, key) => {
                                    const uri = domainurlStrip(value.url);
                                    return (
                                        <Link href={uri} key={key} className={key}>
                                            <a className={`text-white pointer breadcrumbLinks ${key == breadcrumbLength - 1 ? "lastbreadcrumb" : ""}`} >{value.text}</a>
                                        </Link>

                                    )
                                })

                            }
                        </div>
                        <div className="container-lg container-fluid innerPageHeroBanner px-md-5 px-lg-0 px-4 ">
                            <div className="col-xl-6 offset-xl-1 col-lg-7 col-md-9 ">

                                <div dangerouslySetInnerHTML={{ __html: heroBannerInfo?.heroBannerText }} >
                                </div>
                                {
                                    !isEmpty(heroBannerInfo?.heroButtonUrl) &&
                                    <Link href={heroBannerInfo?.heroButtonUrl}>
                                        <a className="btn homeBannerButton" >{heroBannerInfo?.heroButton}</a>
                                    </Link>
                                }
                            </div>
                        </div>
                    </div>
                )
            }
        </>
    )
}

export default Header;
