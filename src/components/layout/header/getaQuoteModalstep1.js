import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, CustomInput, Col, Row } from 'reactstrap';
const GetaQuoteStep1 = () => {
    return (
        <fieldset className="step1">
                    <FormGroup>
                        <Label for="exampleCheckbox">Choose a Service</Label>
                        <div className="form-group col-md-10">
                            <div className="row">
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> TV Wall Mounting
                                </div>
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> Antenna Installation
                                </div>
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> SoundBar Installation
                                </div>
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> Floating TV Cabinet
                                </div>
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> CCTV Installation
                                </div>
                            </div>
                        </div>
                    </FormGroup>
                    <FormGroup className="pt-md-4">
                        <Label for="exampleCheckbox">Accessories</Label>
                        <div className="row">
                            <div className="col-md-6 pt-md-4">
                                <Input type="select" name="select" id="exampleSelect" onChange={handleChangeCategory}>
                                    <option value="">TV Size</option>
                                    <option>TV Wall Mount</option>
                                    <option>Floating TV Cabinet</option>
                                    <option>Soundbar Installation</option>
                                    <option>Antenna Installation</option>
                                    <option>CCTV Installation</option>
                                    <option>Others</option>
                                </Input>
                            </div>
                            <div className="col-md-6 pt-md-4">
                                <Input type="select" name="select" id="exampleSelect" onChange={handleChangeCategory}>
                                    <option value="">TV Brand</option>
                                    <option>TV Wall Mount</option>
                                    <option>Floating TV Cabinet</option>
                                    <option>Soundbar Installation</option>
                                    <option>Antenna Installation</option>
                                    <option>CCTV Installation</option>
                                    <option>Others</option>
                                </Input>
                            </div>
                            <div className="col-md-6 pt-md-4">
                                <Input type="select" name="select" id="exampleSelect" onChange={handleChangeCategory}>
                                    <option value="">Do You Need Brackets</option>
                                    <option>TV Wall Mount</option>
                                    <option>Floating TV Cabinet</option>
                                    <option>Soundbar Installation</option>
                                    <option>Antenna Installation</option>
                                    <option>CCTV Installation</option>
                                    <option>Others</option>
                                </Input>
                            </div>
                            <div className="col-md-6 pt-md-4">
                                <Input type="select" name="select" id="exampleSelect" onChange={handleChangeCategory}>
                                    <option value="">Wall Type</option>
                                    <option>TV Wall Mount</option>
                                    <option>Floating TV Cabinet</option>
                                    <option>Soundbar Installation</option>
                                    <option>Antenna Installation</option>
                                    <option>CCTV Installation</option>
                                    <option>Others</option>
                                </Input>
                            </div>
                        </div>

                    </FormGroup>
                    <button className="btn primaryButton mt-md-3" onClick={handleNextClick} >NEXT</button>
                </fieldset>
    )
}

export default GetaQuoteStep1;