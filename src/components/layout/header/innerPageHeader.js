import { isEmpty } from 'lodash'
import Link from 'next/link';

import MainMenu from "./nav"

const InnerPageHeader = ({header, menus, isFrontPage, heroBannerInfo}) => {

    if(isEmpty({menus})) {
        return null;
    }

    const heroBannerstyling = {
        backgroundImage: `url('${heroBannerInfo?.heroBanner?.sourceUrl}')`
    }

    return (
        <div className={ isFrontPage === true ? "frontPage" : 'frontPage'} style={heroBannerstyling}>
            <MainMenu header={header} menus={ menus } isFrontPage={isFrontPage}/>
            {/* {
                isFrontPage === true ? ( */}
                    <div className="container-fluid container-lg homePageHeroBanner">
                        <div className="col-md-6 offset-md-1">
                            <div dangerouslySetInnerHTML={{__html: heroBannerInfo?.heroBannerText}} >
                            </div>
                            {
                                heroBannerInfo?.heroButtonUrl &&
                                <Link href={heroBannerInfo?.heroButtonUrl}>
                                    <a className="btn homeBannerButton" >{heroBannerInfo?.heroButton}</a>
                                </Link>
                            }
                        </div>
                    </div>
                {/* ) : ''
            } */}
        </div>
    )
}

export default InnerPageHeader;
