import React, { useState, useEffect, useRef } from 'react';
import Link from 'next/link';
import Image from "next/image";

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import ModalExample from './getaQuoteModal';

const MainMenu = ({ header, menus, isFrontPage }) => {
  //console.log(header);

  const [isOpen, setIsOpen] = useState(false);

  const toggle = (e) => {
    setIsOpen(!isOpen);
    var hamburgerTogglerTrue = document.getElementById('hamburgerToggler').innerHTML = '<span class="navbar-toggler-icon"></span>';
    // e.target.className="navbar-toggler-icon";
    // e.target.innerHTML ="";
  }

  //const hamburgerToggler = document.getElementById('hamburgerToggler');

  if (isOpen == true) {
    var hamburgerTogglerTrue = document.getElementById('hamburgerToggler').innerHTML = '<span class="my-1 mx-2 close">X</span>';
    //console.log(hamburgerTogglerTrue)
    //document.getElementById('hamburgerToggler').innerHTML = '<span className="my-1 mx-2 close">X</span>';
  }

  const [navBackground, setNavBackground] = useState(false)
  const navRef = useRef()
  navRef.current = navBackground
  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 10
      if (navRef.current !== show) {
        setNavBackground(show)
      }
    }
    document.addEventListener('scroll', handleScroll)
    return () => {
      document.removeEventListener('scroll', handleScroll)
    }
  }, [])




  return (
    <Navbar style={{ transition: '0.3s ease', backgroundColor: navBackground ? 'rgba(11, 74, 125, 1)' : '', paddingTop: navBackground ? '0px' : '', paddingBottom: navBackground ? '0px' : '' }} expand="lg" className={isFrontPage === true ? "container-custom-large1 container-fluid fixed-top navbar-dark frontpageNavbar navbar-light mobileDark pb-2 pb-md-1 pb-lg-0" : "col-md-12 fixed-top navbar-light bg-light pb-2 pb-md-1 pb-lg-0"}>
      <div className="container-xxl container-xl container-fluid marginnegativemenu">

        <div className="col-xl-1 col-lg-1 col-2 col-sm-2 col-md-3 pe-0 pe-md-5 pe-xl-5 pe-lg-0 mt-md-2 pb-md-2">
          <Link href="/">
            {isFrontPage === true ? (
              <img title={header.siteTitle} src={header.siteLogoUrl} />
            ) : (
              < Image
                src='/sonnie-pro-logo.png'
                // just put the original width and height of the original image, in order to provide the right aspect ratio
                // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                width={70}
                height={70}
                // layout="fill"
                // objectFit="contain"
                alt={header.siteTitle} />

            )
            }
          </Link>
        </div>


        <NavbarToggler onClick={toggle} className="collapsed" id="hamburgerToggler">
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
        <div className="col-xl-8 col-lg-8 col-12 pt-0 pt-md-0 ps-lg-4">
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto pt-2" navbar>

              {
                menus?.length ? (
                  menus.map((menu, key) => (

                    menu?.node?.childItems?.edges?.length ? (
                      <UncontrolledDropdown key={key} nav inNavbar>
                        <DropdownToggle href="/services" className={isFrontPage === true ? "nav-link text-white px-lg-2 px-xl-2 mainNavLink py-1" : "nav-link  px-lg-2 px-xl-2 mainNavLink py-1"} nav caret>
                          {menu?.node?.label}
                        </DropdownToggle>
                        <DropdownMenu className={`dropdown-menu navbarShadow ${isFrontPage === true ? "" : "navbarShadowInner"}`} right >
                          {
                            menu?.node?.childItems?.edges.map((subMenu, key) => (
                              <DropdownItem key={key} className="py-2 pe-5 dropdownNav mainNavLink" >
                                <Link href={subMenu?.node?.path}>
                                  <a className={isFrontPage === true ? "nav-link  text-md-dark  px-md-3  px-lg-2 px-xl-2 mainNavLink pb-1 pb-md-3 pb-lg-0 pb-1" : "nav-link  px-lg-2 px-xl-2 mainNavLink pb-1 pb-md-3 pb-lg-0 pb-1"} onClick={toggle}>{subMenu?.node?.label}</a>
                                </Link>
                              </DropdownItem>
                            )
                            )
                          }
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    ) : (
                      <NavItem key={key}>
                        <Link key={menu?.node?.id} href={menu?.node?.path} >
                          <a className={isFrontPage === true ? "nav-link text-white  px-lg-2 px-xl-2 mainNavLink py-1" : "nav-link  px-lg-2 px-xl-2 mainNavLink py-1"} >{menu?.node?.label}</a>
                        </Link>
                      </NavItem>
                    ))
                  )
                ) : null
              }

            </Nav>
            <div className="col-12 col-lg-3 col-md-4 displayMobile pt-4">
              <div className="row">
                <div className="col-12 col-lg-7 text-start">
                  <small className={isFrontPage === true ? "nav-link1 callusnowHeader text-white" : "callusnowHeader nav-link1"}>CALL US NOW</small>
                  <h5 className="mb-0 headerPhoneCustomFont"><a className={isFrontPage === true ? "callusnowHeaderPhone nav-link1" : "callusnowHeaderPhone nav-link1"} href="tel:0414604307">0414 604 307</a></h5>
                </div>
                <div className="col-12 col-lg-5">
                  <ModalExample info={'OTHER QUOTE'} buttonPosition={'headerSection'} />

                  {/* <span className="btn navGetaQuoteBtn col-md-12 pt-md-2 pb-md-2" >GET A QUOTE</span> */}
                </div>
              </div>
            </div>
          </Collapse>

        </div>
        <div className="col-4 col-lg-3 col-md-4 hideMobile">
          <div className="row centerAlignFlex">
            <div className="col-8 col-lg-7 text-end">
              <small className={isFrontPage === true ? "nav-link1 callusnowHeader text-white" : "callusnowHeader nav-link1"}>CALL US NOW</small>
              <h5 className="mb-0 headerPhoneCustomFont"><a className={isFrontPage === true ? "callusnowHeaderPhone nav-link1" : "callusnowHeaderPhone nav-link1"} href="tel:0414604307">0414 604 307</a></h5>
            </div>
            <div className="col-4 col-lg-5">
              <ModalExample info={'OTHER QUOTE'} buttonPosition={'headerSection'} />

              {/* <span className="btn navGetaQuoteBtn col-md-12 pt-md-2 pb-md-2" >GET A QUOTE</span> */}
            </div>
          </div>
        </div>
      </div>
    </Navbar>
  )
}

export default MainMenu;
