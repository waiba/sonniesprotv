import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, CustomInput, Col, Row } from 'reactstrap';
import styles from './css/getaQuoteModal.module.css'

const ModalExample = ({info, buttonPosition }, props) => {

    const [values, setValues] = useState({
        //step1
        tvWallMounting: '',
        antennaInstallation: '',
        soundBarInstallation: '',
        floatingTVCabinet: '',
        CCTVInstallation: '',
        tvSize: '',
        tvBrand: '',
        doYouNeedbrackets: '',
        wallType: '',

        //step2
        address: '',
        fullName: '',
        fullNameError: false,
        fullNameSuccess: false,
        email: '',
        emailSuccess: false,
        emailError: false,
        phoneNumber: '',
        phoneError: false,
        phoneSuccess: false,
        contactMessage: '',
        messageError: false,
        messageSuccess: false,
        formSuccess: false,
        formMessage: '',
        reload: false
    });

    const { tvWallMounting, antennaInstallation, soundBarInstallation, floatingTVCabinet,CCTVInstallation, tvSize, tvBrand, doYouNeedbrackets, wallType, fullName, fullNameError, fullNameSuccess, email, emailError, emailSuccess, phoneNumber, phoneError, phoneSuccess, contactMessage, messageError, messageSuccess, formSuccess, formMessage, reload} = values;

    const handleChangeEmail = e => {
        setValues( { ...values, email: e.target.value, emailError: false, emailSuccess: false } )
    }

    const handleChangeName = e => {
        setValues( { ...values, fullName: e.target.value, fullNameError: false, fullNameSuccess: false } )
    }

    const handleChangePhone = e => {
        setValues( { ...values, phoneNumber: e.target.value, phoneError: false, phoneSuccess: false } )
    }

    const handleChangeMessage = e => {
        setValues( { ...values, contactMessage: e.target.value, messageError: false, messageSuccess: false } )
    }

    const handleChangeCategory = e => {
        if(e.target.value == ''){
            setValues( { ...values, category: false, categoryError: true, categorySucces: false } )
        } else {
            setValues( { ...values, category: e.target.value, categoryError: false, categorySucces: false } )
        }
    }

    const handleNextClick = e => {
        //e.target.parentNode.parentNode.style.display = "none"
        
        //let a = e.target.parentNode.style.display = "none";
        //document.getElementsByClassName('lastStep').style.display = "block";
        
        e.preventDefault();
        
    }

  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div className="moe">
      
      <a className={(buttonPosition == 'getaQuoteSection') ? `btn primaryButton ${styles.GetaQuoteSection_blueColor}` : `btn navGetaQuoteBtn col-md-12 pt-md-2 pb-md-2`} onClick={toggle}>{info}</a>
      <Modal isOpen={modal} toggle={toggle} className={`${className} modal-lg `}>
          <div className="ps-md-5 pe-md-5">
        {/* <ModalHeader toggle={toggle} className="text-center"> */}
        <div className="text-center pt-md-5" toggle={toggle}>
            <p className="smallSubTitle">Personalize your Quotation</p>
            <h4 className={`${styles.getaQuoteTitle}`}>Get a free Quote</h4>
        </div>
        {/* </ModalHeader> */}
        <ModalBody>
            <Form>
                <fieldset className="step1">
                    <FormGroup>
                        <Label for="exampleCheckbox">Choose a Service</Label>
                        <div className="form-group col-md-10">
                            <div className="row">
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> TV Wall Mounting
                                </div>
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> Antenna Installation
                                </div>
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> SoundBar Installation
                                </div>
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> Floating TV Cabinet
                                </div>
                                <div className="col-md-6">
                                    <input type="checkbox" className="form-check-input" /> CCTV Installation
                                </div>
                            </div>
                        </div>
                    </FormGroup>
                    <FormGroup className="pt-md-4">
                        <Label for="exampleCheckbox">Accessories</Label>
                        <div className="row">
                            <div className="col-md-6 pt-md-4">
                                <Input type="select" name="select" id="exampleSelect" onChange={handleChangeCategory}>
                                    <option value="">TV Size</option>
                                    <option>TV Wall Mount</option>
                                    <option>Floating TV Cabinet</option>
                                    <option>Soundbar Installation</option>
                                    <option>Antenna Installation</option>
                                    <option>CCTV Installation</option>
                                    <option>Others</option>
                                </Input>
                            </div>
                            <div className="col-md-6 pt-md-4">
                                <Input type="select" name="select" id="exampleSelect" onChange={handleChangeCategory}>
                                    <option value="">TV Brand</option>
                                    <option>TV Wall Mount</option>
                                    <option>Floating TV Cabinet</option>
                                    <option>Soundbar Installation</option>
                                    <option>Antenna Installation</option>
                                    <option>CCTV Installation</option>
                                    <option>Others</option>
                                </Input>
                            </div>
                            <div className="col-md-6 pt-md-4">
                                <Input type="select" name="select" id="exampleSelect" onChange={handleChangeCategory}>
                                    <option value="">Do You Need Brackets</option>
                                    <option>TV Wall Mount</option>
                                    <option>Floating TV Cabinet</option>
                                    <option>Soundbar Installation</option>
                                    <option>Antenna Installation</option>
                                    <option>CCTV Installation</option>
                                    <option>Others</option>
                                </Input>
                            </div>
                            <div className="col-md-6 pt-md-4">
                                <Input type="select" name="select" id="exampleSelect" onChange={handleChangeCategory}>
                                    <option value="">Wall Type</option>
                                    <option>TV Wall Mount</option>
                                    <option>Floating TV Cabinet</option>
                                    <option>Soundbar Installation</option>
                                    <option>Antenna Installation</option>
                                    <option>CCTV Installation</option>
                                    <option>Others</option>
                                </Input>
                            </div>
                        </div>

                    </FormGroup>
                    <button className="btn primaryButton mt-md-3" onClick={handleNextClick} >NEXT</button>
                </fieldset>
                <fieldset className={`${styles.step2} lastStep`}>
                    <FormGroup>
                    <Label for="exampleCheckbox">Personal Info</Label>
                    </FormGroup>
                    <Col md={12} className="pb-md-4">
                        <FormGroup>
                            <Input placeholder="Enter your address" type="text" onChange={handleChangePhone} value={phoneNumber} required className={`${styles.fieldStyle}`} />
                        </FormGroup>
                    </Col>
                    <Row>
                        <Col md={6} className="pb-md-4">
                            <FormGroup>
                                <Input placeholder="First Name" type="text"  onChange={handleChangeName} value={fullName} required className={`${styles.fieldStyle}`} />
                            </FormGroup>
                        </Col>
                        <Col md={6} className="pb-md-4">
                            <FormGroup>
                                <Input placeholder="Email" type="email" onChange={handleChangeEmail} value={email} required className={`${styles.fieldStyle}`} />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Col md={12} className="pb-md-4">
                        <FormGroup>
                            <Input placeholder="Phone Number" type="text" onChange={handleChangePhone} value={phoneNumber} required className={`${styles.fieldStyle}`} />
                        </FormGroup>
                    </Col>

                    <Col md={12} className="pb-md-4">
                        <FormGroup>
                            <Input type="textarea" name="text" id="exampleText" onChange={handleChangeMessage} value={contactMessage} rows="4" className={`${styles.fieldStyle}`} placeholder="Write Message" />
                        </FormGroup>
                    </Col>
                </fieldset>
            </Form>
            
        </ModalBody>

        {/* <ModalFooter>
          <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter> */}
        </div>
      </Modal>
    </div>
  );
}

export default ModalExample;