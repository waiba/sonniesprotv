import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, CustomInput, Col, Row } from 'reactstrap';
import fetch from 'isomorphic-unfetch';
import FormData from 'form-data'
//import { useForm } from 'react-hook-form';
import { isEmpty } from "lodash"
import styles from './css/getaQuoteModal.module.css'
import ReCAPTCHA from "react-google-recaptcha"
//import GetaQuoteStep1 from './getaQuoteModalstep1';

const ModalExample = ({ info, buttonPosition }, props) => {


    const [values, setValues] = useState({
        step: 1,

        //step1
        selectService: [
            // { id: 1, value: "TV Wall Mounting", isChecked: false },
            { id: 2, value: "Antenna Installation", isChecked: false },
            { id: 3, value: "SoundBar Installation", isChecked: false },
            { id: 4, value: "Floating TV Cabinet", isChecked: false },
            { id: 5, value: "CCTV Installation", isChecked: false }
        ],
        handleCheckChieldElement: [],
        tvSize: '',
        tvBrand: '',
        doYouNeedbrackets: '',
        wallType: '',

        //step2
        totaladdress: "",
        fullName: '',
        email: '',
        phoneNumber: '',
        contactMessage: '',

        //other messages
        selectServiceError: true,
        totaladdressError: false,
        fullNameError: false,
        emailError: false,
        phoneNumberError: false,
        formMessage: '',
        captchaToken: '',
        captchaTokenError: false,
        reload: false
    });

    const { step, selectService, tvSize, tvBrand, doYouNeedbrackets, wallType, totaladdress, fullName, email, phoneNumber, contactMessage, handleCheckChieldElement, selectServiceError, totaladdressError, fullNameError, emailError, phoneNumberError, formMessage, captchaToken, captchaTokenError } = values;

    const onSubmit = async (e) => {
        e.preventDefault();

        /*if (step === 1) {
            const res = selectService.map((value, key) => {
                if (value.isChecked === true) {
                    return value.isChecked;
                    //return checkService;
                }
            })

            if (res.includes(true)) {
                nextStep();
                //setValues( { ...values, selectServiceError: ""} )
            } else {
                setValues({ ...values, selectServiceError: "Please select at least one Services" })
            }

        }
        if (step === 2) {
            */
        // const serviceRes = selectService.map((value, key) => {
        //     if (value.isChecked === true) {
        //         return value.isChecked;
        //         //return checkService;
        //     }
        // })
        if (selectServiceError == true) {

            setValues({ ...values, selectServiceError: "Please select at least one Services" })
            return
        }

        if (isEmpty(totaladdress)) {
            setValues({ ...values, totaladdressError: "Address is required" })
            return
        }
        if (isEmpty(fullName)) {
            setValues({ ...values, fullNameError: "Name is required" })
            return
        }
        if (isEmpty(email)) {
            setValues({ ...values, emailError: "Email is required" })
            return
        }
        if (isEmpty(phoneNumber)) {
            setValues({ ...values, phoneNumberError: "Phone number is required" })
            return
        }
        if (isEmpty(captchaToken)) {
            setValues({ ...values, captchaTokenError: "Google Captcha error" })
            return;
        }


        const res = await fetch('/api/getaQuote', {
            body: JSON.stringify({
                totaladdress: totaladdress,
                selectService: selectService,
                //handleCheckChieldElement: handleCheckChieldElement,
                tvSize: tvSize,
                tvBrand: tvBrand,
                doYouNeedbrackets: doYouNeedbrackets,
                wallType: wallType,
                fullName: fullName,
                phoneNumber: phoneNumber,
                contactMessage: contactMessage,
                email: email,
                captchaToken: captchaToken
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        }).then(function (response) {
            return response.json();
        });;

        const { error } = res?.error;
        const message = res?.message;


        //console.log(error);

        if (!isEmpty(res?.error)) {
            console.log(res?.error);
            setValues({ ...values, formSuccess: false, formMessage: res?.error })
            return;

        } else {
            if (res?.message?.status == "mail_failed") {
                setValues({ ...values, formSuccess: false, formMessage: res?.message?.message })
                return;
            } else if (res?.message?.status == "validation_failed") {
                setValues({ ...values, formSuccess: false, formMessage: res?.message?.message })
                return;
            }

            if (res?.message?.status == "mail_sent") {
                setValues({
                    ...values, formMessage: res?.message?.message, formSuccess: true,
                    selectService: [
                        // { id: 1, value: "TV Wall Mounting", isChecked: false },
                        { id: 2, value: "Antenna Installation", isChecked: false },
                        { id: 3, value: "SoundBar Installation", isChecked: false },
                        { id: 4, value: "Floating TV Cabinet", isChecked: false },
                        { id: 5, value: "CCTV Installation", isChecked: false }
                    ],
                    handleCheckChieldElement: [],
                    tvSize: '',
                    tvBrand: '',
                    doYouNeedbrackets: '',
                    wallType: '',

                    //step2
                    totaladdress: "",
                    fullName: '',
                    email: '',
                    phoneNumber: '',
                    contactMessage: '',

                    //other messages
                    selectServiceError: false,
                    totaladdressError: false,
                    fullNameError: false,
                    emailError: false,
                    phoneNumberError: false,
                    captchaToken: '',
                    captchaTokenError: false,
                    reload: false
                })
                return;
            }
            //   setValues( { ...values, formMessage:'Form succesfully submitted', formSuccess: true } )
            //   return;

        }



        /*}*/


    };


    const handleChange = input => (e) => {
        setValues({ ...values, [input]: e.target.value, [input + 'Error']: false })
    }



    const handleCheckChieldElements = (event) => {

        selectService.forEach(service => {
            if (service.value === event.target.value) {
                service.isChecked = event.target.checked
            }
        })
        setValues({ ...values, selectService: selectService })

        const serviceRes = selectService.map((value, key) => {
            if (value.isChecked === true) {
                return value.isChecked;
                //return checkService;
            }
        })

        if (serviceRes.includes(true)) {
            setValues({ ...values, selectServiceError: false })
        } else {
            setValues({ ...values, selectServiceError: true })
        }

    }

    const onChangeCaptcha = value => {
        setValues({ ...values, captchaToken: value })
    }



    const showError = () => {
        if (selectServiceError !== false) {
            return <p className="text-danger">{selectServiceError}</p>
        }

        if (totaladdressError !== false) {
            return <p className="text-danger">{totaladdressError}</p>
        }

        if (fullNameError !== false) {
            return <p className="text-danger">{fullNameError}</p>
        }

        if (emailError !== false) {
            return <p className="text-danger">{emailError}</p>
        }

        if (phoneNumberError !== false) {
            return <p className="text-danger">{phoneNumberError}</p>
        }
        if (totaladdressError !== false) {
            return <p className="text-danger">{totaladdressError}</p>
        }

        if (fullNameError !== false) {
            return <p className="text-danger">{fullNameError}</p>
        }

        if (emailError !== false) {
            return <p className="text-danger">{emailError}</p>
        }

        if (phoneNumberError !== false) {
            return <p className="text-danger">{phoneNumberError}</p>
        }
        if (captchaTokenError) {
            return <p className="text-danger">{captchaTokenError}</p>
        }
    }
    // const showErrorSecondStep = () => {

    //     if (totaladdressError !== false) {
    //         return <p className="text-danger">{totaladdressError}</p>
    //     }

    //     if (fullNameError !== false) {
    //         return <p className="text-danger">{fullNameError}</p>
    //     }

    //     if (emailError !== false) {
    //         return <p className="text-danger">{emailError}</p>
    //     }

    //     if (phoneNumberError !== false) {
    //         return <p className="text-danger">{phoneNumberError}</p>
    //     }
    //     if (captchaTokenError) {
    //         return <p className="text-danger">{captchaTokenError}</p>
    //     }
    // }



    // const handleNextClick = e => {
    //     nextStep();

    // }

    // const handlePreviousClick = e => {
    //     prevStep();
    // }

    // const showStep = () => {
    //     if (step === 1) {
    //         return (
    //             <fieldset className="step1">
    //                 <FormGroup>
    //                     <Label className={`${styles.label} pb-3 pb-md-4`}>Choose a Service</Label>


    //                     <div className="form-group col-lg-10">
    //                         <div className="row">
    //                             {
    //                                 selectService.map((value, key) => {
    //                                     return (
    //                                         <div className={`col-md-6 pb-2 pb-md-3 ${styles.checkboxFont}`} key={key}>
    //                                             <input onChange={handleCheckChieldElements} name="handleCheckChieldElement" id={key} type="checkbox" checked={value.isChecked} value={value.value} className="form-check-input mb-1" /> <label className="form-check-label ms-2" htmlFor={key}>{value.value}</label>
    //                                         </div>
    //                                     )
    //                                 })
    //                             }
    //                         </div>
    //                     </div>
    //                 </FormGroup>
    //                 <FormGroup className="pt-md-4">
    //                     <Label className={`${styles.label} pb-3 pb-md-4`}>Accessories</Label>
    //                     <div className="row">
    //                         <div className="col-md-6 pb-3">
    //                             <select type="select" name="tvSize" id="tvSize" className="form-control form-select" onChange={handleChange('tvSize')} value={tvSize}>
    //                                 <option value="">TV Size</option>
    //                                 <option>Up to 65"</option>
    //                                 <option>Above 65</option>
    //                             </select>
    //                         </div>
    //                         <div className="col-md-6 pb-3">
    //                             <select type="select" name="tvBrand" id="tvBrand" className="form-control form-select" onChange={handleChange('tvBrand')} value={tvBrand}>
    //                                 <option value="">TV Brand</option>
    //                                 <option>LG</option>
    //                                 <option>Panasonic</option>
    //                                 <option>Samsung</option>
    //                                 <option>Sony</option>
    //                                 <option>TCL</option>
    //                                 <option>Kogan</option>
    //                                 <option>Hisense</option>
    //                                 <option>Soniq</option>
    //                                 <option>Other</option>
    //                             </select>
    //                         </div>
    //                         <div className="col-md-6 pb-3">
    //                             <select type="select" name="doYouNeedbrackets" id="doYouNeedbrackets" className="form-control form-select" onChange={handleChange('doYouNeedbrackets')} value={doYouNeedbrackets}>
    //                                 <option value="">Do You Need Brackets</option>
    //                                 <option>Yes</option>
    //                                 <option>No</option>
    //                             </select>
    //                         </div>
    //                         <div className="col-md-6 pb-3">
    //                             <select type="select" name="wallType" id="wallType" className="form-control form-select" onChange={handleChange('wallType')} value={wallType}>
    //                                 <option value="">Wall Type</option>
    //                                 <option>Not Sure</option>
    //                                 <option>Wooden Studs</option>
    //                                 <option>Steel Studs</option>
    //                                 <option>Brick/Concrete</option>
    //                                 <option>Gyprok with Timber</option>
    //                                 <option>Other</option>
    //                             </select>
    //                         </div>
    //                     </div>

    //                 </FormGroup>

    //                 <div className="row">
    //                     <div className="col-md-12 text-end">
    //                         <button className="btn primaryButton mt-3 mt-md-3 submit " type="submit" >
    //                             {
    //                                 (step === 2) ? 'Submit' : 'Next'
    //                             }
    //                         </button>
    //                     </div>
    //                 </div>

    //             </fieldset>
    //         )
    //     }

    //     if (step === 2) {
    //         return (
    //             <fieldset className={`lastStep`}>
    //                 <FormGroup>
    //                     <Label className={`${styles.label} pb-3 pb-md-4`}>Personal Info</Label>
    //                 </FormGroup>
    //                 <Col md={12} className="pb-3 pb-md-4">
    //                     {/* <FormGroup> */}
    //                     <input placeholder="Enter your address" type="text" name="totaladdress" className="form-control" onChange={handleChange('totaladdress')} value={totaladdress} />

    //                     {/* </FormGroup> */}
    //                 </Col>
    //                 <Row>
    //                     <Col md={6} className="pb-3 pb-md-4">
    //                         <FormGroup>
    //                             <input placeholder="First Name" type="text" name="fullName" className="form-control" onChange={handleChange('fullName')} value={fullName} />
    //                         </FormGroup>
    //                     </Col>
    //                     <Col md={6} className="pb-3 pb-md-4">
    //                         <FormGroup>
    //                             <input placeholder="Email" type="email" name="email" className="form-control" onChange={handleChange('email')} value={email} />
    //                         </FormGroup>
    //                     </Col>
    //                 </Row>
    //                 <Col md={12} className="pb-3 pb-md-4">
    //                     <FormGroup>
    //                         <input placeholder="Phone Number" type="text" name="phoneNumber" className="form-control" onChange={handleChange('phoneNumber')} value={phoneNumber} />
    //                     </FormGroup>
    //                 </Col>

    //                 <Col md={12} className="pb-3 pb-md-4">
    //                     <FormGroup>
    //                         <textarea name="contactMessage" id="contactmessage" rows="4" className="form-control" placeholder="Write Message" onChange={handleChange('contactMessage')} value={contactMessage}></textarea>
    //                     </FormGroup>
    //                 </Col>

    //                 <ReCAPTCHA sitekey={process.env.NEXT_PUBLIC_GOOGLE_CAPTCHA_SITE_KEY} onChange={onChangeCaptcha} />

    //                 {showErrorSecondStep()}

    //                 <div className="row">
    //                     <div className="col-6 col-md-6">
    //                         <button className="btn primaryButton mt-3 mt-md-3 previous" onClick={handlePreviousClick} >Previous</button>
    //                     </div>
    //                     <div className="col-6 col-md-6 text-end">
    //                         <button className="btn primaryButton mt-3 mt-md-3 submit " type="submit" >
    //                             {
    //                                 (step === 2) ? 'Submit' : 'Next'
    //                             }
    //                         </button>
    //                     </div>
    //                 </div>

    //             </fieldset>

    //         )
    //     }
    // }

    // const nextStep = () => {
    //     setValues({ ...values, step: step + 1, selectServiceError: "" })

    // }

    // const prevStep = () => {
    //     setValues({ ...values, step: step - 1 })
    // }

    const {
        buttonLabel,
        className
    } = props;

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    return (
        <div className="moe pt-3 pt-md-0">
            <a className={(buttonPosition == 'getaQuoteSection') ? `btn primaryButton me-md-4 me-sm-4 me-2 mb-2 mb-md-0 ${styles.GetaQuoteSection_blueColor}` : `d-none`} href="/get-a-quote/">TV Installation Quote</a>
            <a className={(buttonPosition == 'getaQuoteSection') ? `btn primaryButton mb-2 mb-md-0 ${styles.GetaQuoteSection_blueColor}` : `btn navGetaQuoteBtn col-12 col-md-12 pt-md-2 pb-md-2 mb-2 mb-lg-0`} onClick={toggle}>{info}</a>
            <Modal isOpen={modal} toggle={toggle} className={`${className} modal-lg modalContainer`}>
                <ModalHeader toggle={toggle}></ModalHeader>

                <div className=" px-lg-5 px-md-0 px-4 ">

                    {/* <ModalHeader toggle={toggle} className="text-center"> */}

                    <div className="text-center" toggle={toggle}>
                        <p className="smallSubTitle">Personalize your Quotation</p>
                        <h4 className={`${styles.getaQuoteTitle} pb-4`}>Get a free Quote</h4>
                    </div>
                    {/* </ModalHeader> */}
                    <ModalBody>
                        <form onSubmit={onSubmit} className="pb-4">
                            {/* {showStep()} */}
                            <fieldset className="step1">
                                <FormGroup>
                                    <Label className={`${styles.label} pb-3 pb-md-4`}>Choose a Service</Label>


                                    <div className="form-group col-lg-10">
                                        <div className="row">
                                            {
                                                selectService.map((value, key) => {
                                                    return (
                                                        <div className={`col-md-6 pb-2 pb-md-3 ${styles.checkboxFont}`} key={key}>
                                                            <input onChange={handleCheckChieldElements} name="handleCheckChieldElement" id={key} type="checkbox" checked={value.isChecked} value={value.value} className="form-check-input mb-1" /> <label className="form-check-label ms-2" htmlFor={key}>{value.value}</label>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                </FormGroup>
                            </fieldset>
                            <fieldset className={`lastStep`}>
                                <FormGroup>
                                    <Label className={`${styles.label} pb-3 pb-md-4`}>Personal Info</Label>
                                </FormGroup>
                                <Col md={12} className="pb-3 pb-md-4">
                                    {/* <FormGroup> */}
                                    <input placeholder="Enter your address" type="text" name="totaladdress" className="form-control" onChange={handleChange('totaladdress')} value={totaladdress} />

                                    {/* </FormGroup> */}
                                </Col>
                                <Row>
                                    <Col md={6} className="pb-3 pb-md-4">
                                        <FormGroup>
                                            <input placeholder="First Name" type="text" name="fullName" className="form-control" onChange={handleChange('fullName')} value={fullName} />
                                        </FormGroup>
                                    </Col>
                                    <Col md={6} className="pb-3 pb-md-4">
                                        <FormGroup>
                                            <input placeholder="Email" type="email" name="email" className="form-control" onChange={handleChange('email')} value={email} />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Col md={12} className="pb-3 pb-md-4">
                                    <FormGroup>
                                        <input placeholder="Phone Number" type="text" name="phoneNumber" className="form-control" onChange={handleChange('phoneNumber')} value={phoneNumber} />
                                    </FormGroup>
                                </Col>

                                <Col md={12} className="pb-3 pb-md-4">
                                    <FormGroup>
                                        <textarea name="contactMessage" id="contactmessage" rows="4" className="form-control" placeholder="Write Message" onChange={handleChange('contactMessage')} value={contactMessage}></textarea>
                                    </FormGroup>
                                </Col>

                                <ReCAPTCHA sitekey={process.env.NEXT_PUBLIC_GOOGLE_CAPTCHA_SITE_KEY} onChange={onChangeCaptcha} />


                                <div className="row">
                                    <div className="col-12">
                                        <button className="btn primaryButton mt-3 mt-md-3 submit " type="submit" >
                                            Submit
                                        </button>
                                    </div>
                                </div>

                            </fieldset>

                            {showError()}
                            <p>{formMessage}</p>
                        </form>
                    </ModalBody>

                    {/* <ModalFooter>
          <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter> */}
                </div>
            </Modal>
        </div>
    );
}

export default ModalExample;
