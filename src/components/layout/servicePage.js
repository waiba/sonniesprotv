import Header from './header'
import Footer from './footer'
import GetaQuote from '../home/GetaQuoteSection'
import Subscribe from './Subscribe' 
import ServicesIntro from '../servicesInnerPage/ServicesIntro'
import ServicesFeatures from '../servicesInnerPage/ServicesFeatures'
import ServicesHowItWorks from '../servicesInnerPage/ServicesHowItWorks'
import ServicesWhyWorkWithUs from '../servicesInnerPage/ServicesWhyWorkWithUs'
import ServicesBenefitWorkingWithUs from '../servicesInnerPage/ServicesBenefitWorkingWithUs'

const ServicePageLayout = ({data, children}) => {
    return (
        <div>
            <Header header={data?.header} menus={data?.menus?.mainMenus} isFrontPage={data?.page?.isFrontPage} heroBannerInfo={data?.page?.acfHomePageBanner} />
    
            <div className="pageContent">
                <ServicesIntro acfServicesIntroSection={data?.page?.acfServicesIntroSection}/>
                <ServicesFeatures acfServicesFeatureSection={data?.page?.acfServicesFeatureSection}/>
                <ServicesHowItWorks acfServicesProcessSection={data?.page?.acfServicesProcessSection}/>
                <ServicesWhyWorkWithUs acfServicesWhyWorkWithUs={data?.page?.acfServicesWhyWorkWithUs} />
                <ServicesBenefitWorkingWithUs acfServicesBenefitWorkingWithUs={data?.page?.acfServicesBenefitWorkingWithUs} bgColor={{'color':'background: rgba(11, 74, 125, 0.05);'}} />
                <GetaQuote acfGetaQuote={data?.homePage?.acfGetaQuote} />
                <Subscribe acfContactInfo={data?.homePage?.acfContactInfo}/>
            </div>
            
            {children}
            <Footer footer={data?.footer} />
        </div>
    )
}

export default ServicePageLayout;