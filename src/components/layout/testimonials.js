import Header from "./header";
import Footer from "./footer"
import GetaQuote from "../home/GetaQuoteSection";
import Subscribe from "./Subscribe";
import TestimonialsBody from "../testimonials";
//import jquery from 'jquery'; 

const TestimonialsLayout = ({data, children}) => {
    //console.log(data?.page?.content);
    return(
        <div>
            <Header header={data?.header} menus={data?.menus?.mainMenus} isFrontPage={data?.page?.isFrontPage} heroBannerInfo={data?.page?.acfHomePageBanner} />

            {
                data?.homePage && 
                    <div className="pageContent">
                        <TestimonialsBody TestimonialsBody={data?.testimonials}/>
                        <GetaQuote acfGetaQuote={data?.homePage?.acfGetaQuote} />
                        <Subscribe acfContactInfo={data?.homePage?.acfContactInfo}/>
                    </div>
            }
            
            
            <div dangerouslySetInnerHTML={{__html: data?.page?.content}}>
                
            </div>
            {children}
            <Footer footer={data?.footer} />
        </div>
    )
}

export default TestimonialsLayout;