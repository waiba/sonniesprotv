//import {isEmpty} from 'lodash'
import Image from "next/image";
const Footer = ({ footer }) => {
    if (footer) {
        return (
            <div className="container-fluid footer px-md-5 px-lg-0 px-4">
                <div className="container-lg container-fluid  ">
                    <div className="row pb-5">
                        <div className="sidebarFooter1 col-md-6 col-lg-3 col-sm-12 mb-md-5 mb-lg-0 mb-4 ">
                            <div dangerouslySetInnerHTML={{ __html: footer?.sidebarOne }} ></div>
                        </div>
                        <div className="sidebarFooter1 col-md-6 col-lg-3 col-sm-12  mb-md-5 mb-lg-0 mb-4">
                            <div dangerouslySetInnerHTML={{ __html: footer?.sidebarTwo }} ></div>
                        </div>
                        <div className="sidebarFooter1 col-md-6 col-lg-3 col-sm-12 mb-md-5 mb-lg-0 mb-4">
                            <div dangerouslySetInnerHTML={{ __html: footer?.sidebarThree }} ></div>
                        </div>
                        <div className="sidebarFooter1 col-md-6 col-lg-3 col-sm-12 mb-md-5 mb-lg-0 mb-4">
                            <div className="pb-4" dangerouslySetInnerHTML={{ __html: footer?.sidebarFour }} ></div>
                            <a href="https://www.google.com/maps/place/SPROTV-+TV+wall+mounting+and+Installations/@-33.8482439,150.9319747,10z/data=!4m5!3m4!1s0x0:0x655741cd1ee52627!8m2!3d-33.8482439!4d150.9319747" target="_blank"><Image
                                src="/google-reviews-five-star.png"
                                height={61}
                                width={200}
                                alt="Sonnies Pro Tv google review"
                            /></a>
                        </div>
                    </div>
                    <div className="row pt-3">
                        <div className="col-md-6">
                            <p>Copyright 2020 Sonnies Pro TV All rights reserved.</p>
                        </div>
                        <div className="col-md-6 text-md-end text-start">
                            <p>ABN: 63 675 408 451 &nbsp; TITAB: T53877 &nbsp; Licence Number: 000107959</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;
