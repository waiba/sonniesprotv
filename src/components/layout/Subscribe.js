import React, { useState } from 'react';
import Image from 'next/image';
//import React, { useRef, useState, useEffect } from 'react';
import fetch from 'isomorphic-unfetch';
import { isEmpty } from 'lodash'
//import { InputGroup, InputGroupAddon, InputGroupText, Input, Form, Row, Col, FormGroup, Label, FormFeedback, FormText  } from 'reactstrap';
import { InputGroup, InputGroupText, Input, Form, Col, FormGroup } from 'reactstrap';
//import {PropTypes} from 'prop-types'
import Link from 'next/link';
// import {
//   Heading,
//   InputGroup,
//   Box,
//   Input,
//   InputRightElement,
//   Button,
//   Text,
//   useToast,
//   useColorMode
// } from '@chakra-ui/core';

const Subscribe = ({ acfContactInfo }) => {


    const [values, setValues] = useState({
        name: '',
        nameError: false,
        nameSuccess: false,
        email: '',
        emailSuccess: false,
        emailError: false,
        formSuccess: '',
        reload: false
    });

    const { name, nameError, nameSuccess, email, emailError, emailSuccess, formSuccess, reload } = values;

    const handleChangeEmail = e => {
        setValues({ ...values, email: e.target.value, emailError: false, emailSuccess: false })
    }

    const handleChangeName = e => {
        setValues({ ...values, name: e.target.value, nameError: false, nameSuccess: false })
    }

    const showError = () => {
        if (nameError) {
            return <p className="text-danger">{nameError}</p>
        }
        if (emailError) {
            return <p className="text-danger">{emailError}</p>
        }
    }

    const formMessage = () => {
        if (formSuccess) {
            return <p className="text-danger">Subscribed</p>
        }
        else {
            return <p className="text-danger">Something went wrong</p>
        }
    }



    //const toast = useToast();
    //const { colorMode } = useColorMode();
    const bgColor = {
        light: 'blue.50',
        dark: 'blue.900'
    };
    const borderColor = {
        light: 'blue.200',
        dark: 'blue.900'
    };

    const subscribe = async (e) => {
        e.preventDefault();
        setValues({ ...values, formSuccess: "" })
        if (isEmpty(email)) {
            //console.log('empty valye');
            setValues({ ...values, emailError: "Please enter the valid email address" })
            return;
        }

        if (isEmpty(name)) {
            //console.log('empty valye');
            setValues({ ...values, nameError: "Name field is required" })
            return;
        }

        const res = await fetch('/api/subscribe', {
            body: JSON.stringify({
                email: email
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        });

        const { error } = await res.json();

        if (error) {
            //   toast({
            //     title: 'An error occurred.',
            //     description: error,
            //     status: 'error',
            //     duration: 3000,
            //     isClosable: true
            //   });
            setValues({ ...values, formSuccess: error })
            return;

        } else {

            setValues({ ...values, formSuccess: 'form succesfully submitted' })
            return;
        }

        // toast({
        //   title: 'Success!',
        //   description: 'You are now subscribed.',
        //   status: 'success',
        //   duration: 3000,
        //   isClosable: true
        // });
    };

    return (
        <div className="subscribeSection container-lg container-fluid px-md-5 px-lg-0 px-4">
            <div className="row">
                <div className="col-md-12 col-lg-4">
                    <div className="pb-md-4 pb-4 pb-3">
                        {!isEmpty(acfContactInfo?.logo?.sourceUrl) &&
                            <Image
                                src={acfContactInfo?.logo?.sourceUrl}
                                // just put the original width and height of the original image, in order to provide the right aspect ratio
                                // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                width={80}
                                height={80}
                                // layout="fill"
                                // objectFit="contain"
                                alt={acfContactInfo?.logo?.altText} />
                        }
                    </div>
                    <p className="addressInfo mb-md-4" dangerouslySetInnerHTML={{ __html: acfContactInfo?.address }}></p>
                    <p className="contactPhone mb-md-4"><Link href={`tel:${acfContactInfo?.phone}`}>{acfContactInfo?.phone}</Link></p>
                    <p className="contactEmail mb-md-4"><Link href={`mailto:${acfContactInfo?.emailAddress}`}>{acfContactInfo?.emailAddress}</Link></p>
                    <div className="openingDays" dangerouslySetInnerHTML={{ __html: acfContactInfo?.openingDays }}></div>
                </div>
                <div className="col-md-12 col-lg-8 envelopeWrapper mt-md-5 mt-5 mt-lg-0">
                    <div className="col-lg-10 offset-lg-1 col-md-12 px-md-4 px-lg-0 px-3">
                        <h4 className="h4Title pb-md-4">Subscribe to our Newsletter</h4>
                        <Form className="pt-md-3">
                            <div className="row g-3">
                                <Col md={6}>
                                    <FormGroup className="">
                                        <Input placeholder="Your Name" type="text" onChange={handleChangeName} value={name} required className="py-2" />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>
                                    <FormGroup className="">
                                        <Input placeholder="user@email.com" type="email" onChange={handleChangeEmail} value={email} required className="py-2" />
                                    </FormGroup>
                                </Col>
                            </div>

                            {showError()}

                            <InputGroup className="pt-md-3">
                                <button className="btn primaryButton mt-md-3 mt-3" onClick={subscribe}>
                                    Subscribe
                                </button>
                            </InputGroup>
                            {
                                formSuccess != '' &&
                                <p>{formSuccess}</p>
                            }

                        </Form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Subscribe;
