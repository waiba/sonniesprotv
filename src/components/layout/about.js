import Header from "./header";
import Footer from "./footer"
import GetaQuote from "../home/GetaQuoteSection";
import Subscribe from "./Subscribe";
import WhoWeAre from "../about/WhoWeAre";
import OurMission from "../about/OurMission";
import AboutpageWhyChooseUs from "../about/AboutpageWhyChooseUs";
import OurCoreValues from "../about/OurCoreValues";
//import jquery from 'jquery'; 

const AboutLayout = ({data, children}) => {
    //console.log(data?.page?.content);
    
    return(
        <div>
            <Header header={data?.header} menus={data?.menus?.mainMenus} isFrontPage={data?.page?.isFrontPage} heroBannerInfo={data?.page?.acfHomePageBanner} />
            {/* <div className="container">
                <img src="http://localhost/sonniesprotv/assets/landscape-3639879_1920.jpg" className="img-responsive" width="100%"/>
            </div> */}

            {
                data?.homePage && 
                    <div className="pageContent">
                        <WhoWeAre acfWhoWeAre={data?.page?.acfAboutPageWhoWeAre} />
                        <OurMission acfOurMisson={data?.page?.acfAboutPageOurMission} />
                        <AboutpageWhyChooseUs acfAboutPageWhyChooseUs={data?.page?.acfAboutPageWhyChooseUs} />
                        <OurCoreValues acfAboutOurCoreValues={data?.page?.acfAboutPageOurCoreValues} />
                        <GetaQuote acfGetaQuote={data?.homePage?.acfGetaQuote} />
                        <Subscribe acfContactInfo={data?.homePage?.acfContactInfo}/>
                    </div>
            }
            
            
            <div dangerouslySetInnerHTML={{__html: data?.page?.content}}>
                
            </div>
            {children}
            <Footer footer={data?.footer} />
        </div>
    )
}

export default AboutLayout;