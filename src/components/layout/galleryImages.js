import React, { useState, useCallback } from "react";
import Gallery from "react-photo-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";

const GalleryImages = ({acfGallery}) => {
  const [currentImage, setCurrentImage] = useState(0);
  const [viewerIsOpen, setViewerIsOpen] = useState(false);

  const openLightbox = useCallback((event, { photo, index }) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);

  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };
  
  const photosJson = (acfGallery) => {
    var jsonFile = [];
    acfGallery?.galleryImages.map((val, key) => {
      var jsonFile1 = {
        src: val.src,
        height: val.mediaDetails.height,
        width: val.mediaDetails.width
       }
       jsonFile.push(jsonFile1);
    })
    return jsonFile;
  }

  const refinedFalleryImages = photosJson(acfGallery);

  return (
    <div className="faqSection container-lg container-fluid px-4 px-md-5 px-lg-0 sectionPadding120">
      <Gallery photos={refinedFalleryImages} direction={"column"} onClick={openLightbox} />
      <ModalGateway>
        {viewerIsOpen ? (
          <Modal onClose={closeLightbox}>
            <Carousel
              currentIndex={currentImage}
              views={refinedFalleryImages.map(x => ({
                ...x,
                srcset: x.srcSet,
                caption: x.title
              }))}
            />
          </Modal>
        ) : null}
      </ModalGateway>
    </div>
  );
}
export default GalleryImages;