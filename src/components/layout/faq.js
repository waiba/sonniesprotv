import { isEmpty } from 'lodash'
const FaqLayout = ({ faqs }) => {

    if (isEmpty(faqs)) {
        return null;
    }
    return (
        <div className="faqSection container-lg container-fluid px-4 px-md-5 px-lg-0 sectionPadding120">
            <div className="col-xxl-10 offset-xxl-1  col-md-12">
                <div className="row gx-0">
                    {
                        faqs.map((value, key) => (

                            <div className="col-12 col-md-6 px-xl-6 ps-md-5 pb-md-5 pb-4 px-0 px-md-4 singleFaqWrap" key={key}>
                                <h5 className="faqSingleTitle IconBeforetitle pt-2  ps-sm-5 pe-sm-4">{value.faqSingleTitle}</h5>
                                <div className="faqSingleContent pt-3   ps-md-5 pe-sm-4" dangerouslySetInnerHTML={{ __html: value.faqSingleContent }}></div>

                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}

export default FaqLayout;
