import React, { useRef, useState, useEffect } from 'react';
//import { Helmet } from 'react-helmet'
import fetch from 'isomorphic-unfetch';
import { isEmpty } from 'lodash'
import { InputGroup, InputGroupAddon, InputGroupText, Input, Form, Row, Col, FormGroup, Label, FormFeedback, FormText } from 'reactstrap';
import styles from './css/GetaQuoteForm.module.css'
import ReCAPTCHA from "react-google-recaptcha"

const GetaQuoteForm = () => {


    const [values, setValues] = useState({
        fullName: '',
        fullNameError: false,
        fullNameSuccess: false,
        email: '',
        emailSuccess: false,
        emailError: false,
        phoneNumber: '',
        phoneError: false,
        phoneSuccess: false,
        suburb: '',
        suburbError: false,
        suburbSuccess: false,
        tvSize: '',
        tvSizeError: false,
        tvSizeSuccess: false,
        haveBracket: [
            { id: 1, value: "Yes", isChecked: false },
            { id: 2, value: "No", isChecked: false },
        ],
        haveBracketError: false,
        haveBracketSuccess: false,
        whatBracket: [
            { id: 1, value: "Flat", isChecked: false },
            { id: 2, value: "Tilt", isChecked: false },
            { id: 3, value: "Full-Motion", isChecked: false },
            { id: 4, value: "Ceiling", isChecked: false },
            { id: 5, value: "Don't Know", isChecked: false },
        ],
        whatBracketError: false,
        whatBracketSuccess: false,
        wallType: [
            { id: 1, value: "Brick", isChecked: false },
            { id: 2, value: "Gyprock", isChecked: false },
            { id: 3, value: "Concrete", isChecked: false },
            { id: 4, value: "Other", isChecked: false },
            { id: 5, value: "Don't Know", isChecked: false },
        ],
        wallTypeError: false,
        wallTypeSuccess: false,
        hiddenCables: [
            { id: 1, value: "Yes", isChecked: false },
            { id: 2, value: "No", isChecked: false },
        ],
        hiddenCablesError: false,
        hiddenCablesSuccess: false,
        isThis: [
            { id: 1, value: "House", isChecked: false },
            { id: 2, value: "Apartment", isChecked: false },
            { id: 3, value: "Unit", isChecked: false },
            { id: 4, value: "Villa", isChecked: false },
            { id: 5, value: "Other (Leave a Note)", isChecked: false },
        ],
        isThisError: false,
        isThisSuccess: false,
        additionalInfo: '',
        additionalInfoError: false,
        additionalInfoSuccess: false,
        captchaToken: '',
        captchaTokenError: false,
        formSuccess: false,
        formMessage: '',
        reload: false
    });

    const { fullName, fullNameError, fullNameSuccess, email, emailError, emailSuccess, phoneNumber, phoneError, phoneSuccess,
        suburb,
        suburbError,
        suburbSuccess,
        tvSize,
        tvSizeError,
        tvSizeSuccess,
        haveBracket,
        haveBracketError,
        haveBracketSuccess,
        whatBracket,
        whatBracketError,
        whatBracketSuccess,
        wallType,
        wallTypeError,
        wallTypeSuccess,
        hiddenCables,
        hiddenCablesError,
        hiddenCablesSuccess,
        isThis,
        isThisError,
        isThisSuccess,
        additionalInfo,
        additionalInfoError,
        additionalInfoSuccess,
        captchaToken, captchaTokenError, messageError, messageSuccess, formSuccess, formMessage, reload } = values;

    const handleChangeEmail = e => {
        setValues({ ...values, email: e.target.value, emailError: false, emailSuccess: false })
    }

    const handleChangeName = e => {
        setValues({ ...values, fullName: e.target.value, fullNameError: false, fullNameSuccess: false })
    }

    const handleChangePhone = e => {
        setValues({ ...values, phoneNumber: e.target.value, phoneError: false, phoneSuccess: false })
    }

    const handleChangeSuburb = e => {
        setValues({ ...values, suburb: e.target.value, suburbError: false, suburbSuccess: false })
    }

    const handleChangeTVsize = e => {
        setValues({ ...values, tvSize: e.target.value, tvSizeError: false, tvSizeSuccess: false })
    }

    const handleCheckhaveBracket = (event) => {
        haveBracket.forEach(bracket => {

            if (bracket.value === event.target.value)
                bracket.isChecked = event.target.checked
        })
        if (!haveBracket.includes(true)) {
            setValues({ ...values, haveBracket: haveBracket, haveBracketError: false, haveBracketSuccess: false });
        }
        //setValues({ ...values, haveBracket: haveBracket })
    }



    const handleCheckwhatBracket = (event) => {

        whatBracket.forEach(bracket => {
            if (bracket.value === event.target.value)
                bracket.isChecked = event.target.checked
        })
        setValues({ ...values, whatBracket: whatBracket })
    }

    const handleCheckwallType = (event) => {

        wallType.forEach(type => {
            if (type.value === event.target.value)
                type.isChecked = event.target.checked
        })

        if (!wallType.includes(true)) {
            setValues({ ...values, wallType: wallType, wallTypeError: false, wallTypeSuccess: false });
        }
    }

    const handleCheckhiddenCables = (event) => {
        hiddenCables.forEach(cables => {
            if (cables.value === event.target.value)
                cables.isChecked = event.target.checked
        })

        if (!hiddenCables.includes(true)) {
            setValues({ ...values, hiddenCables: hiddenCables, hiddenCablesError: false, hiddenCablesSuccess: false });
        }

    }

    const handleCheckisThis = (event) => {
        isThis.forEach(cables => {
            if (cables.value === event.target.value)
                cables.isChecked = event.target.checked
        })

        if (!isThis.includes(true)) {
            setValues({ ...values, isThis: isThis, isThisError: false, isThisSuccess: false });
        }

    }

    const handleChangeAdditionalInfo = e => {
        setValues({ ...values, additionalInfo: e.target.value, additionalInfoError: false, additionalInfoSuccess: false })
    }

    const onChangeCaptcha = value => {
        setValues({ ...values, captchaToken: value })
    }

    const showError = () => {
        if (fullNameError) {
            return <p className="text-danger">{fullNameError}</p>
        }
        if (emailError) {
            return <p className="text-danger">{emailError}</p>
        }
        if (phoneError) {
            return <p className="text-danger">{phoneError}</p>
        }
        if (suburbError) {
            return <p className="text-danger">{suburbError}</p>
        }
        if (tvSizeError) {
            return <p className="text-danger">{tvSizeError}</p>
        }
        if (haveBracketError) {
            return <p className="text-danger">{haveBracketError}</p>
        }
        if (whatBracketError) {
            return <p className="text-danger">{whatBracketError}</p>
        }
        if (wallTypeError) {
            return <p className="text-danger">{wallTypeError}</p>
        }
        if (hiddenCablesError) {
            return <p className="text-danger">{hiddenCablesError}</p>
        }
        if (isThisError) {
            return <p className="text-danger">{isThisError}</p>
        }
        // if (additionalInfoError) {
        //     return <p className="text-danger">{additionalInfoError}</p>
        // }
        if (captchaTokenError) {
            return <p className="text-danger">{captchaTokenError}</p>
        }
    }

    // const formMessage = () => {
    //     if(formSuccess){
    //         return <p className="text-danger">Message Sent</p>
    //     }
    //     else {
    //         return <p className="text-danger">Something went wrong</p>
    //     }
    // }



    //const toast = useToast();
    //const { colorMode } = useColorMode();
    const bgColor = {
        light: 'blue.50',
        dark: 'blue.900'
    };
    const borderColor = {
        light: 'blue.200',
        dark: 'blue.900'
    };

    const formSubmit = async (e) => {
        e.preventDefault();
        setValues({ ...values, formSuccess: "" })



        if (isEmpty(email)) {
            //console.log('empty valye');
            setValues({ ...values, emailError: "Please enter the valid email address" })
            return;
        }

        if (isEmpty(fullName)) {
            //console.log('empty valye');
            setValues({ ...values, fullNameError: "Name field is required" })
            return;
        }

        if (isEmpty(phoneNumber)) {
            //console.log('empty valye');
            setValues({ ...values, phoneError: "Phone number is required" })
            return;
        }

        if (isEmpty(suburb)) {
            //console.log('empty valye');
            setValues({ ...values, suburbError: "Suburb is required" })
            return;
        }

        if (isEmpty(tvSize)) {
            //console.log('empty valye');
            setValues({ ...values, tvSizeError: "TV Size is required" })
            return;
        }


        const haveBracketRes = haveBracket.map((value, key) => {
            if (value.isChecked === true) {
                return value.isChecked;
                //return checkService;
            }
        })


        if (!haveBracketRes.includes(true)) {
            setValues({ ...values, haveBracketError: "Please select Do you have bracket field" });
            return;
        }

        //var fields = array('haveBracket', 'wallType', 'hiddenCables');

        const wallTypeRes = wallType.map((value, key) => {
            if (value.isChecked === true) {
                return value.isChecked;
                //return checkService;
            }
        })

        if (!wallTypeRes.includes(true)) {
            setValues({ ...values, wallTypeError: "Please select Wall Type" });
            return

        }


        const hiddenCablesRes = hiddenCables.map((value, key) => {
            if (value.isChecked === true) {
                return value.isChecked;
                //return checkService;
            }
        })


        if (hiddenCablesRes.includes(true)) {
            //setValues( { ...values, selectServiceError: ""} )
        } else {
            setValues({ ...values, hiddenCablesError: "Please select hidden cables field" });
            return
        }

        const isThisRes = isThis.map((value, key) => {
            if (value.isChecked === true) {
                return value.isChecked;
                //return checkService;
            }
        })


        if (isThisRes.includes(true)) {
            //setValues( { ...values, selectServiceError: ""} )
        } else {
            setValues({ ...values, isThisError: "Please select is this field" });
            return
        }

        // if (isEmpty(additionalInfo)) {
        //     setValues({ ...values, additionalInfoError: "Message is required" })
        //     return;
        // }

        if (isEmpty(captchaToken)) {
            setValues({ ...values, captchaTokenError: "Google Captcha error" })
            return;
        }

        // if(!isEmpty(captchaToken)) {
        //     setValues( { ...values, captchaTokenError: false} )
        //     return;
        // }

        const body = JSON.stringify({
            fullName: fullName,
            phoneNumber: phoneNumber,
            additionalInfo: additionalInfo,
            phoneNumber: phoneNumber,
            email: email,
            suburb: suburb,
            tvSize: tvSize,
            haveBracket: haveBracket,
            whatBracket: whatBracket,
            wallType: wallType,
            hiddenCables: hiddenCables,
            isThis: isThis,
            captchaToken: captchaToken
        });




        const res = await fetch('/api/get-a-quote', {
            body: JSON.stringify({
                fullName: fullName,
                phoneNumber: phoneNumber,
                additionalInfo: additionalInfo,
                phoneNumber: phoneNumber,
                email: email,
                suburb: suburb,
                tvSize: tvSize,
                haveBracket: haveBracket,
                whatBracket: whatBracket,
                wallType: wallType,
                hiddenCables: hiddenCables,
                isThis: isThis,
                captchaToken: captchaToken
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        }).then(function (response) {
            return response.json();
        });;

        const { error } = res?.error;
        const message = res?.message;



        if (!isEmpty(res?.error)) {
            setValues({ ...values, formSuccess: false, formMessage: res?.error })
            return;

        } else {
            if (res?.message?.status == "mail_failed") {
                setValues({ ...values, formSuccess: false, formMessage: res?.message?.message })
                return;
            } else if (res?.message?.status == "validation_failed") {
                setValues({ ...values, formSuccess: false, formMessage: res?.message?.message })
                return;
            }

            if (res?.message?.status == "mail_sent") {
                document.getElementById('getAquoteForm').reset();
                setValues({
                    ...values, formMessage: res?.message?.message, formSuccess: true,
                    fullName: '',
                    fullNameError: false,
                    fullNameSuccess: false,
                    email: '',
                    emailSuccess: false,
                    emailError: false,
                    phoneNumber: '',
                    phoneError: false,
                    phoneSuccess: false,
                    additionalInfo: '',
                    additionalInfoError: false,
                    additionalInfoSuccess: false,
                    captchaToken: '',
                    captchaTokenError: false,
                    reload: false,
                    suburb: '',
                    suburbError: false,
                    suburbSuccess: false,
                    tvSize: '',
                    tvSizeError: false,
                    tvSizeSuccess: false,

                    haveBracket: [
                        { id: 1, value: "Yes", isChecked: false },
                        { id: 2, value: "No", isChecked: false },
                    ],
                    whatBracket: [
                        { id: 1, value: "Flat", isChecked: false },
                        { id: 2, value: "Tilt", isChecked: false },
                        { id: 3, value: "Full-Motion", isChecked: false },
                        { id: 4, value: "Ceiling", isChecked: false },
                        { id: 5, value: "Don't Know", isChecked: false },
                    ],
                    whatBracketError: false,
                    whatBracketSuccess: false,
                    wallType: [
                        { id: 1, value: "Brick", isChecked: false },
                        { id: 2, value: "Gyprock", isChecked: false },
                        { id: 3, value: "Concrete", isChecked: false },
                        { id: 4, value: "Other", isChecked: false },
                        { id: 5, value: "Don't Know", isChecked: false },
                    ],
                    wallTypeError: false,
                    wallTypeSuccess: false,
                    hiddenCables: [
                        { id: 1, value: "Yes", isChecked: false },
                        { id: 2, value: "No", isChecked: false },
                    ],
                    hiddenCablesError: false,
                    hiddenCablesSuccess: false,
                    isThis: [
                        { id: 1, value: "House", isChecked: false },
                        { id: 2, value: "Apartment", isChecked: false },
                        { id: 3, value: "Unit", isChecked: false },
                        { id: 4, value: "Villa", isChecked: false },
                        { id: 5, value: "Other (Leave a Note)", isChecked: false },
                    ],
                })
                return;
            }
            //   setValues( { ...values, formMessage:'Form succesfully submitted', formSuccess: true } )
            //   return;

        }



    };


    return (
        <div>
            {/* <Helmet>
                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
            </Helmet> */}
            <Form className="pt-3" id="getAquoteForm">
                <Row>
                    <Col md={6} className="pb-md-4 pb-3">
                        <FormGroup>
                            <label className="mb-2">Name *</label><br />
                            <Input name="fullName" type="text" onChange={handleChangeName} value={fullName} required className={`${styles.fieldStyle}`} />
                        </FormGroup>
                    </Col>
                    <Col md={6} className="pb-md-4 pb-3">
                        <FormGroup>
                            <label className="mb-2">Email *</label><br />
                            <Input type="email" onChange={handleChangeEmail} value={email} required className={`${styles.fieldStyle}`} />
                        </FormGroup>
                    </Col>
                </Row>

                <Row>
                    <Col md={6} className="pb-md-4 pb-3">
                        <FormGroup>
                            <label className="mb-2">Phone Number *</label><br />
                            <Input type="text" onChange={handleChangePhone} value={phoneNumber} required className={`${styles.fieldStyle}`} />
                        </FormGroup>
                    </Col>

                    <Col md={6} className="pb-md-4 pb-3"  >
                        <FormGroup>
                            <label className="mb-2">Suburb *</label><br />
                            <Input type="text" onChange={handleChangeSuburb} value={suburb} required className={`${styles.fieldStyle}`} />
                        </FormGroup>
                    </Col>
                </Row>

                <Row>
                    <Col md={6} className="pb-md-4 pb-3">
                        <FormGroup>
                            <label className="mb-2">TV Size *</label><br />
                            <Input type="text" onChange={handleChangeTVsize} value={tvSize} required className={`${styles.fieldStyle}`} />
                        </FormGroup>
                    </Col>

                    <Col md={6} className="pb-md-4 pb-3"  >
                        <FormGroup>
                            <label className="mb-2">Do you already have a Bracket?</label><br />
                            {
                                haveBracket.map((value, key) => {
                                    return (
                                        <div className={`pb-2 pb-md-3 pe-3 d-inline ${styles.checkboxFont}`} key={key}>
                                            <input onChange={handleCheckhaveBracket} name="handleCheckhaveBracket" id={`handleCheckhaveBracket-${key}`} type="checkbox" checked={value.isChecked} value={value.value} className="form-check-input mb-1" required /> <label for={`handleCheckhaveBracket-${key}`} class="form-check-label ms-2" htmlFor={key}>{value.value}</label>
                                        </div>
                                    )
                                })
                            }
                        </FormGroup>
                    </Col>
                </Row>
                <Col md={12} className="pb-md-4 pb-3">
                    <FormGroup>
                        <label className="mb-2">What kind of bracket do you have or intend to have?</label><br />
                        {
                            whatBracket.map((value, key) => {
                                return (
                                    <div className={`pb-2 pb-md-3 pe-3 d-inline ${styles.checkboxFont}`} key={key}>
                                        <input onChange={handleCheckwhatBracket} name="handleCheckwhatBracket" id={`handleCheckwhatBracket-${key}`} type="checkbox" checked={value.isChecked} value={value.value} className="form-check-input mb-1" /> <label for={`handleCheckwhatBracket-${key}`} class="form-check-label ms-2" htmlFor={key}>{value.value}</label>
                                    </div>
                                )
                            })
                        }
                    </FormGroup>
                </Col>

                <Col md={12} className="pb-md-4 pb-3">
                    <FormGroup>
                        <label className="mb-2">Wall Type</label><br />
                        {
                            wallType.map((value, key) => {
                                return (
                                    <div className={`pb-2 pb-md-3 pe-3 d-inline ${styles.checkboxFont}`} key={key}>
                                        <input onChange={handleCheckwallType} name="handleCheckwallType" id={`handleCheckwallType-${key}`} type="checkbox" checked={value.isChecked} value={value.value} required className="form-check-input mb-1" /> <label for={`handleCheckwallType-${key}`} class="form-check-label ms-2" htmlFor={key}>{value.value}</label>
                                    </div>
                                )
                            })
                        }
                    </FormGroup>
                </Col>


                <Col md={12} className="pb-md-4 pb-3">
                    <FormGroup>
                        <label className="mb-2">Do you want hidden cables?</label><br />
                        {
                            hiddenCables.map((value, key) => {
                                return (
                                    <div className={`pb-2 pb-md-3 pe-3 d-inline ${styles.checkboxFont}`} key={key}>
                                        <input onChange={handleCheckhiddenCables} name="handleCheckhiddenCables" id={`handleCheckhiddenCables-${key}`} type="checkbox" checked={value.isChecked} value={value.value} required className="form-check-input mb-1" /> <label for={`handleCheckhiddenCables-${key}`} class="form-check-label ms-2" htmlFor={key}>{value.value}</label>
                                    </div>
                                )
                            })
                        }
                    </FormGroup>
                </Col>

                <Col md={12} className="pb-md-4 pb-3">
                    <FormGroup>
                        <label className="mb-2">Is this?</label><br />
                        {
                            isThis.map((value, key) => {
                                return (
                                    <div className={`pb-2 pb-md-3 pe-3 d-inline ${styles.checkboxFont}`} key={key}>
                                        <input onChange={handleCheckisThis} name="handleCheckisThis" id={`handleCheckisThis-${key}`} type="checkbox" checked={value.isChecked} value={value.value} required className="form-check-input mb-1" /> <label for={`handleCheckisThis-${key}`} class="form-check-label ms-2" htmlFor={key}>{value.value}</label>
                                    </div>
                                )
                            })
                        }
                    </FormGroup>
                </Col>

                <Col md={12} className="pb-md-4 pb-3">
                    <FormGroup>
                        <label className="mb-2">Additional Information</label><br />
                        <Input type="textarea" name="text" id="exampleText" rows="8" onChange={handleChangeAdditionalInfo} className={`${styles.fieldStyle}`} />
                    </FormGroup>
                </Col>

                <ReCAPTCHA sitekey={process.env.NEXT_PUBLIC_GOOGLE_CAPTCHA_SITE_KEY} onChange={onChangeCaptcha} />

                {showError()}
                <br />
                <InputGroup>
                    <button className="btn primaryButton mt-3" onClick={formSubmit} >
                        Submit
                    </button>
                </InputGroup>
                {
                    formSuccess == true &&
                    <p>{formMessage}</p>
                }
                {
                    formSuccess == false &&
                    <p>{formMessage}</p>
                }


            </Form>
        </div>
    )
}

export default GetaQuoteForm;
