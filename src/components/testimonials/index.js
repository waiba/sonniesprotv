import styles from './css/index.module.css'
import TestimonialPageBlock from "./testimonial-block";

const TestimonialsBody = ({ TestimonialsBody }) => {
    console.log(TestimonialsBody);
    return (
        <div className={`${styles.contactBodySection} container-lg container-fluid  px-md-5 px-4 px-lg-0`}>
            <h3 className="sectionTitle text-center col-md-8 mx-auto pb-md-5 pb-3">5 <i class="fa fa-star"></i> customer service with a guaranteed craftsmanship</h3>
            <TestimonialPageBlock TestimonialPageBlock={TestimonialsBody} />
        </div>
    )
}

export default TestimonialsBody;
