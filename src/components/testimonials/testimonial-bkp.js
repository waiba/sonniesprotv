import { isEmpty } from "lodash";
import Image from "next/image";
import { Helmet } from "react-helmet";
import Styles from './css/testimonial-block.module.css'

const TestimonialPageBlock = ({ TestimonialPageBlock }) => {

    const testimonialPlatform = (value) => {
        if (value == 'Google') {
            return (
                <img src="/google.png" />
            )
        }
        else {
            return (
                <img src="/fb-icon.png" />
            )
        }
    }

    const testimonialStar = (value) => {
        var i = 1;
        var img = "";
        while (i <= value) {
            img += '<img src="/star.png" class="displayInline" />';
            i++;
        }
        return img;
    }



    return (
        <div className={`testimonialSection container`}>

            <Helmet>
                <script id="masonryScript" src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async />
            </Helmet>

            <div className="testimonialsBlockWrap row" data-masonry='{"percentPosition": true }'>
                {
                    TestimonialPageBlock?.edges.map((value, key) => {


                        return (

                            <div className="col-md-6 mt-md-1 mb-md-4 pe-md-4" key={key} >
                                <div className={` p-5 customShadow shadowTestimonial`}>
                                    <div className="row">
                                        <div className="col-md-2 col-12 text-center text-md-start mb-5 mb-md-0">

                                            <div className={`${Styles.roundedImage}`}>
                                                {!isEmpty(value?.node?.featuredImage?.node?.sourceUrl) &&
                                                    < Image
                                                        src={value?.node?.featuredImage?.node?.sourceUrl}
                                                        // just put the original width and height of the original image, in order to provide the right aspect ratio
                                                        // Next.js will automatically reduce the size if the rendered image needs to be smaller.
                                                        width={150}
                                                        height={150}
                                                        // layout="fill"
                                                        // objectFit="contain"
                                                        alt={value?.node?.featuredImage?.node?.altText} />
                                                }
                                            </div>
                                        </div>
                                        <div className="col-md-10 col-12">
                                            <div dangerouslySetInnerHTML={{ __html: value?.node?.content }} className={`text-center text-md-start ${Styles.singleTestimonialWrapContent}`}>
                                            </div>
                                            <div className={`col-md-12 ${Styles.starTestimonial}`} dangerouslySetInnerHTML={{ __html: testimonialStar(value?.node?.acfTestimonialInfo?.starRating) }}>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-10">
                                                    <h4 className={Styles.testimonialName}>
                                                        {value?.node?.title}
                                                    </h4>
                                                    <p className={Styles.tetimonialLocation}>
                                                        {value?.node?.acfTestimonialInfo?.location}
                                                    </p>
                                                </div>
                                                <div className="col-md-2 text-md-end">
                                                    {testimonialPlatform(value?.node?.acfTestimonialInfo?.reviewPlatform)}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })
                }
            </div>
        </div>
    )
}

export default TestimonialPageBlock;
