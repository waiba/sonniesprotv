import React, { Component } from 'react';

const JobDetails = () => {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }

        const { jobTitle, jobCompany, jobLocation, handleChange } = this.props;
        return(
            <div>
                <h2>Enter your job information:</h2>
                <label>
                    <input
                        type="text"
                        name="jobTitle"
                        value={jobTitle}
                        onChange={handleChange('jobTitle')}
                        placeholder="Job Title"
                    />
                </label>
                <label>
                    <input
                        type="text"
                        name="jobCompany"
                        value={jobCompany}
                        onChange={handleChange('jobCompany')}
                        placeholder="Company"
                    />
                </label>
                <label>
                    <input
                        type="text"
                        name="jobCompany"
                        value={jobLocation}
                        onChange={handleChange('jobLocation')}
                        placeholder="Location"
                    />
                </label>
                <button className="Back" onClick={back}>
                    « Back
                </button>
                <button className="Next" onClick={continue}>
                    Next »
                </button>
            </div>
        );
}

export default JobDetails;
