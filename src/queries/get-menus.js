import { gql } from "@apollo/client"
import MenuFragment from "./fragments/menus";

export const HeaderFooter = `
  header: getHeader {
    favicon
    siteTagLine
    siteLogoUrl
    siteTitle
  }
  mainMenus: menuItems(where: {location: PRIMARY, parentId: "0"}) {
    edges {
      node {
        ...MenuFragment
        childItems {
          edges {
            node {
              ...MenuFragment
            }
          }
        }
      }
    }
  }
  footer: getFooter {
    copyrightText
    sidebarOne
    sidebarTwo
    sidebarThree
    sidebarFour
    socialLinks {
      iconName
      iconUrl
    }
  }
`

export const GET_MENUS = gql`
query GET_MENUS {
  ${HeaderFooter}
}
  ${MenuFragment}
`