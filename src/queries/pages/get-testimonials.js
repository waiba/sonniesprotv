import { gql } from "@apollo/client";
import MenuFragment from "../fragments/menus";
import { HeaderFooter } from "../get-menus";
import SeoFragment from "../fragments/seo"

export const GET_TESTIMONIALS = gql`
	query GET_TESTIMONIALS {
      ${HeaderFooter}
	  page: pageBy(pageId: 35) {
	    id
	    title
	    content
	    slug
		uri
		acfHomePageBanner {
			fieldGroupName
			heroBannerText
			heroButton
			heroButtonUrl
			heroBanner {
				altText
				sourceUrl(size: HEROBANNERIMAGE)
			}
		}
		acfContactUsFields {
			callOurExpertButtonText
			callOurExpertContent
			callOurExpertTitle
			chatOnlineButtonText
			chatOnlineContent
			chatOnlineTitle
		  }
		acfGetaQuote {
			getAQuoteBackgroundImage {
				altText
				sourceUrl(size: GETAQUOTEBANNERIMAGE)
			}
			getAQuoteContent
			getAQuoteLinkText
			getAQuoteTitle
		}
		seo{
			...SeoFragment
		  }
	  }
	  homePage: pageBy(pageId: 27) {
        acfContactInfo {
          address
          emailAddress
          openingDays
          phone
          logo {
            altText
            sourceUrl(size: LARGE)
          }
		}
		acfGetaQuote {
			getAQuoteBackgroundImage {
				altText
				sourceUrl(size: GETAQUOTEBANNERIMAGE)
			}
			getAQuoteContent
			getAQuoteLinkText
			getAQuoteTitle
		}
      }
      testimonials: cptTestimonials(first: 40) {
        edges {
          node {
            content(format: RENDERED)
            title(format: RENDERED)
            acfTestimonialInfo {
              location
              reviewPlatform
              starRating
            }
            featuredImage {
              node {
                altText
                sourceUrl(size: THUMBNAIL)
              }
            }
          }
        }
      }
	}
	${MenuFragment}
	${SeoFragment}
`;