import { gql } from "@apollo/client";
import MenuFragment from "../fragments/menus";
import {HeaderFooter} from "../get-menus";
import SeoFragment from "../fragments/seo"

export const GET_PAGE = gql`
	query GET_PAGE($uri: String) {
      ${HeaderFooter}
	  page: pageBy(uri: $uri) {
	    id
	    title
	    content
	    slug
		uri
		acfHomePageBanner {
			heroBannerText
			heroButton
			heroButtonUrl
			heroBanner {
				altText
				sourceUrl(size: HEROBANNERIMAGE)
			}
		}
		acfFaqs {
			faqSingle {
				faqSingleContent
				faqSingleTitle
			  }
		}
		acfGetaQuote {
			getAQuoteBackgroundImage {
				altText
				sourceUrl(size: LARGE)
			}
			getAQuoteContent
			getAQuoteLinkText
			getAQuoteTitle
		}
		acfGallery {
			galleryImages {
				mediaDetails {
					height
					width
					src: file
				  }
				  src: sourceUrl
			}
		}
		seo{
			...SeoFragment
		}
	  }
	  homePage: pageBy(pageId: 27) {
        acfContactInfo {
          address
          emailAddress
          openingDays
          phone
          logo {
            altText
            sourceUrl(size: LARGE)
          }
		}
		acfGetaQuote {
			getAQuoteBackgroundImage {
				altText
				sourceUrl(size: GETAQUOTEBANNERIMAGE)
			}
			getAQuoteContent
			getAQuoteLinkText
			getAQuoteTitle
		}
      }
	}
	${MenuFragment}
	${SeoFragment}
`;