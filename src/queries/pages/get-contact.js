import { gql } from "@apollo/client";
import MenuFragment from "../fragments/menus";
import {HeaderFooter} from "../get-menus";
import SeoFragment from "../fragments/seo"

export const GET_CONTACT = gql`
	query GET_CONTACT {
      ${HeaderFooter}
	  page: pageBy(pageId: 37) {
	    id
	    title
	    content
	    slug
		uri
		acfHomePageBanner {
			fieldGroupName
			heroBannerText
			heroButton
			heroButtonUrl
			heroBanner {
				altText
				sourceUrl(size: HEROBANNERIMAGE)
			}
		}
		acfContactUsFields {
			callOurExpertButtonText
			callOurExpertContent
			callOurExpertTitle
			chatOnlineButtonText
			chatOnlineContent
			chatOnlineTitle
		  }
		acfGetaQuote {
			getAQuoteBackgroundImage {
				altText
				sourceUrl(size: LARGE)
			}
			getAQuoteContent
			getAQuoteLinkText
			getAQuoteTitle
		}
		seo{
			...SeoFragment
		  }
	  }
	  homePage: pageBy(pageId: 27) {
        acfContactInfo {
          address
          emailAddress
          openingDays
          phone
          logo {
            altText
            sourceUrl(size: LARGE)
          }
		}
		acfGetaQuote {
			getAQuoteBackgroundImage {
				altText
				sourceUrl(size: GETAQUOTEBANNERIMAGE)
			}
			getAQuoteContent
			getAQuoteLinkText
			getAQuoteTitle
		}
      }
	}
	${MenuFragment}
	${SeoFragment}
`;