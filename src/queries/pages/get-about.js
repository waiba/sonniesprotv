import { gql } from '@apollo/client'
import MenuFragment from "../fragments/menus";
import { HeaderFooter } from '../get-menus'
import SeoFragment from "../fragments/seo"

export const GET_ABOUT = gql`
	query GET_ABOUT {
      ${HeaderFooter}
      page: pageBy(pageId: 29) {
        id
        content
        isFrontPage
        title
        uri
        acfHomePageBanner {
          fieldGroupName
          heroBannerText
          heroButton
          heroButtonUrl
          heroBanner {
            altText
            sourceUrl(size: HEROBANNERIMAGE)
          }
        }
        acfAboutPageWhoWeAre {
          mainContent
          mainTitleText
          smallTitleText
          singleBlocksWrap {
            singleContent
            singleIcon {
              altText
              sourceUrl(size: LARGE)
            }
            singleTitle
          }
        }
        acfAboutPageOurMission {
          missionContent
          missionTitle
        }
        acfAboutPageWhyChooseUs {
          whyChooseUsMainContent
          whyChooseUsMainTitleText
          whyChooseUsSmallTitleText
          whyChooseUsImage {
            altText
            sourceUrl(size: LARGE)
          }
        }
        acfAboutPageOurCoreValues {
          coreValuesMainContent
          coreValuesMainTitleText
          coreValuesSingleBlocksWrap {
            singleContent
            singleTitle
            singleIcon {
              altText
              sourceUrl(size: LARGE)
            }
          }
        }
        acfGetaQuote {
          getAQuoteBackgroundImage {
            altText
            sourceUrl(size: LARGE)
          }
          getAQuoteContent
          getAQuoteLinkText
          getAQuoteTitle
        }
        seo{
          ...SeoFragment
        }

      }
      homePage: pageBy(pageId: 27) {
        acfGetaQuote {
          getAQuoteBackgroundImage {
            altText
            sourceUrl(size: GETAQUOTEBANNERIMAGE)
          }
          getAQuoteContent
          getAQuoteLinkText
          getAQuoteTitle
        }
        acfContactInfo {
          address
          emailAddress
          openingDays
          phone
          logo {
            altText
            sourceUrl(size: LARGE)
          }
        }
      }
	}
  ${MenuFragment}
  ${SeoFragment}
`;