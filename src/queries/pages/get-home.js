import { gql } from '@apollo/client'
import MenuFragment from "../fragments/menus";
import { HeaderFooter } from '../get-menus'
import SeoFragment from "../fragments/seo"

// export const GET_HOME = gql`{
//     query GET_HOME($uri: String) {
//         ${HeaderFooter}
//         pageBy(uri: $uri) {
//             id
//             title
//             content
//             slug
//             uri
//             acfHomePageBanner {
//                 fieldGroupName
//                 heroBanner {
//                 altText
//                 id
//                 uri
//                 }
//                 heroBannerText
//                 heroButton
//                 heroButtonUrl
//             }
//         }
//     }
//   }
//   ${MenuFragment}
//   `;

export const GET_HOME = gql`
	query GET_HOME {
      ${HeaderFooter}
	  page: pageBy(pageId: 27) {
        id
        content
        isFrontPage
        title
        uri
        acfHomePageBanner {
          fieldGroupName
          heroBannerText
          heroButton
          heroButtonUrl
          heroBanner {
            altText
            sourceUrl(size: HEROBANNERIMAGE)
          }
        }
        acfHeroSlider {
          fieldGroupName
          slides {
            slideGroup {
              content
              image {
                altText
                sourceUrl(size: HEROBANNERIMAGE)
              }
            }
          }
        }
        acfHomePageWhyChooseUs {
          singleBlocksWrap {
            fieldGroupName
            singleContent
            singleTitle
            singleIcon {
              altText
              sourceUrl(size: LARGE)
            }
          }
          smallTitleText
          mainTitleText
          mainContent
          mainContentUrlText
          mainContentLink {
            url
          }
        }
        acfServices {
          serviceMainTitleText
          serviceSmallTitleText
          serviceSingleBlock {
            linkText
            serviceContent
            serviceTitle
            serviceImage {
              altText
              sourceUrl(size: LARGE)
            }
            serviceLink {
              title
              url
            }
          }
        }
        acfCounterBlock {
          counterTitle
          singleCounterBlock {
            title
            totalNumber
          }
        }
        acf2MinuteVideo {
          videoContentLink
          videoContentUrlText
          videoMainContent
          videoMainTitleText
          videoSmallTitleText
        }
        acfInstagramFeed {
          instagramFeed
        }
        acfGetaQuote {
          getAQuoteBackgroundImage {
            altText
            sourceUrl(size: GETAQUOTEBANNERIMAGE)
          }
          getAQuoteContent
          getAQuoteLinkText
          getAQuoteTitle
        }
        acfContactInfo {
          address
          emailAddress
          openingDays
          phone
          logo {
            altText
            sourceUrl(size: LARGE)
          }
        }
        seo{
          ...SeoFragment
        }
      }
      testimonials: cptTestimonials(first: 24) {
        edges {
          node {
            content(format: RENDERED)
            title(format: RENDERED)
            acfTestimonialInfo {
              location
              reviewPlatform
              starRating
            }
            featuredImage {
              node {
                altText
                sourceUrl(size: THUMBNAIL)
              }
            }
          }
        }
      }
      ourassociates: cptOurAssociates(first:20) {
        edges {
          node {
            title(format: RENDERED)
            featuredImage {
              node {
                altText
                sourceUrl(size: LARGE)
                srcSet(size: LARGE)
              }
            }
          }
        }
      }
	}
  ${MenuFragment}
  ${SeoFragment}
`;