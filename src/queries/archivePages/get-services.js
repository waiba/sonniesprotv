import { gql } from '@apollo/client'

/**
 * Get Service pages.
 *
 */
export const GET_SERVICES_URI = gql`
 query GET_SERVICES_URI {
  servicePages: cptServices {
    nodes {
      id
      uri
      slug
    }
  }
 }
 `;