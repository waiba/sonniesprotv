import { gql } from "@apollo/client";
import MenuFragment from "../fragments/menus";
import {HeaderFooter} from "../get-menus";
import SeoFragment from "../fragments/seo"

export const GET_SERVICE = gql`
	query GET_SERVICE($uri: String) {
      ${HeaderFooter}
	  servicePage: cptServiceBy(uri: $uri) {
	    id
	    title
	    content
	    slug
        uri
        acfHomePageBanner {
            heroBannerText
            heroButton
            heroButtonUrl
            heroBanner {
            altText
            sourceUrl(size: HEROBANNERIMAGE)
            }
        }
        acfGetaQuote {
            getAQuoteBackgroundImage {
                altText
                sourceUrl(size: GETAQUOTEBANNERIMAGE)
            }
            getAQuoteContent
            getAQuoteLinkText
            getAQuoteTitle
        }
        acfServicesBenefitWorkingWithUs {
          servicesBenefitWorkingWithUsBlock {
              benefitSingleBlockContent
              benefitSingleBlockTitle
              benefitSingleBlockImage {
              altText
              sourceUrl(size: LARGE)
              }
            }
            servicesBenefitWorkingWithUsContent
            servicesBenefitWorkingWithUsTitle
          }
        acfServicesFeatureSection {
            servicesFeatureSmallTitle
            servicesFeatureTitle
            serviceFeatureBlock {
              featureSingleBlock
            }
        }
        acfServicesIntroSection {
            servicesIntroContent
            servicesIntroTitle
        }
        acfServicesProcessSection {
            serviceFeatureContent
            serviceProcessSmallTitle
            serviceProcessTitle
            serviceProcessBlock {
              serviceProcessSingle
            }
          }
        acfServicesWhyWorkWithUs {
            whyWorkWithUsContent
            whyWorkWithUsSmallTitle
            whyWorkWithUsTitle
            whyWorkWithUsImage {
              altText
              sourceUrl(size: LARGE)
            }
        }
        acfOtherServices {
          otherServicesSectionContent
          otherServicesSectionTitle
        }
        seo{
          ...SeoFragment
        }
      }
      homePage: pageBy(pageId: 27) {
        acfContactInfo {
          address
          emailAddress
          openingDays
          phone
          logo {
            altText
            sourceUrl(size: LARGE)
          }
        }
        acfGetaQuote {
          getAQuoteBackgroundImage {
            altText
            sourceUrl(size: GETAQUOTEBANNERIMAGE)
          }
          getAQuoteContent
          getAQuoteLinkText
          getAQuoteTitle
        }
      }
	}
  ${MenuFragment}
  ${SeoFragment}
`;