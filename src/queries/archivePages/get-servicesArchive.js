import { gql } from '@apollo/client'
import MenuFragment from "../fragments/menus";
import { HeaderFooter } from '../get-menus'
import SeoFragment from "../fragments/seo"

export const GET_SERVICESARCHIVE = gql`
	query GET_SERVICESARCHIVE {
      ${HeaderFooter}
      page: pageBy(pageId: 247) {
        id
        title
        content
        slug
        uri
        acfHomePageBanner {
          fieldGroupName
          heroBannerText
          heroButton
          heroButtonUrl
          heroBanner {
            altText
            sourceUrl(size: HEROBANNERIMAGE)
          }
        }
        acfGetaQuote {
          getAQuoteBackgroundImage {
            altText
            sourceUrl(size: GETAQUOTEBANNERIMAGE)
          }
          getAQuoteContent
          getAQuoteLinkText
          getAQuoteTitle
        }
        seo{
          ...SeoFragment
        }
      }
      homePage: pageBy(pageId: 27) {
        acfContactInfo {
          address
          emailAddress
          openingDays
          phone
          logo {
            altText
            sourceUrl(size: LARGE)
          }
        }
      }
      services: cptServices(first: 10) {
        edges {
          node {
            slug
            title(format: RENDERED)
            featuredImage {
              node {
                altText
                sourceUrl(size: LARGE)
                title(format: RENDERED)
              }
            }
            excerpt(format: RENDERED)
            uri
          }
        }
      }
	}
  ${MenuFragment}
  ${SeoFragment}
`;