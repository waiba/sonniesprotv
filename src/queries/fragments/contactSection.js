const ContactSectionFragment = `
fragment ContactSectionFragment on ContactSectionFragmentItem {
    address
    emailAddress
    openingDays
    phone
    logo {
        altText
        sourceUrl(size: LARGE)
    }
}
`

export default ContactSectionFragment;