const GetaQuoteSectionFragment = `
fragment GetaQuoteSectionFragment on GetaQuoteSectionItem {
    getAQuoteBackgroundImage {
      altText
      sourceUrl(size: LARGE)
    }
    getAQuoteContent
    getAQuoteLinkText
    getAQuoteTitle
}
`

export default GetaQuoteSectionFragment;