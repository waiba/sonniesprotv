const ServiceFragment = `
fragment ServiceFragment on ServiceItem {
    content(format: RENDERED)
    slug
    title(format: RENDERED)
    status
    featuredImage {
        node {
        altText
        sourceUrl(size: LARGE)
        }
    }
}
`

export default ServiceFragment;