const MenuFragment = `
fragment MenuFragment on MenuItem {
    id
    label
    path
    url
    locations
}
`

export default MenuFragment;