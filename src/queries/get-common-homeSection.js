import { gql } from "@apollo/client"
import GetaQuoteSectionFragment from "./fragments/getaQuoteSection";
import ContactSectionFragment from "./fragments/contactSection";

export const HomeCommonSection = `
    homePage: pageBy(pageId: 27) {
        acfGetaQuote {
            ...GetaQuoteSectionFragment
        }
        acfContactInfo {
            ...ContactSectionFragment
        }
    }
`

export const GET_COMMONHOMESECTION = gql`
query GET_COMMONHOMESECTION {
  ${HomeCommonSection}
}
  ${GetaQuoteSectionFragment}
  ${ContactSectionFragment}
`