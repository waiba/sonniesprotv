import client from "../../src/apollo/client";
//import Head from 'next/head'
import { GET_SERVICESARCHIVE } from "../../src/queries/archivePages/get-servicesArchive";
//import { isEmpty } from 'lodash'
import ArchivePages from "../../src/components/layout/ArchivePages";
export default function Index({ data }) {

  return (
    <ArchivePages data={data}>
    </ArchivePages>
  )
}


export async function getStaticProps(context) {

  const { data, loading, networkStatus } = await client.query({
    query: GET_SERVICESARCHIVE
  });

  return {
    props: {
      data: {
        header: data?.header || [],
        menus: {
          mainMenus: data?.mainMenus?.edges
        },
        page: data?.page ?? {},
        homePage: data?.homePage ?? {},
        services: data?.services ?? {},
        footer: data?.footer || []
      }
    },
    revalidate: 1 // incremental static regeneration time in seconds
  }
}
