import { useState } from "react";
import { css } from "@emotion/react";
import ScaleLoader from "react-spinners/ScaleLoader";
import Layout from "../../src/components/layout";
import { isEmpty } from 'lodash'
import client from "../../src/apollo/client";
import { GET_SERVICES_URI } from "../../src/queries/archivePages/get-services";
import { GET_SERVICE } from "../../src/queries/archivePages/get-service";
import { useRouter } from 'next/router'
//import ServicePageLayout from "../../src/components/layout/servicePage";
import ServicesIntro from "../../src/components/servicesInnerPage/ServicesIntro";
import ServicesFeatures from "../../src/components/servicesInnerPage/ServicesFeatures";
import ServicesHowItWorks from "../../src/components/servicesInnerPage/ServicesHowItWorks";
import ServicesWhyWorkWithUs from "../../src/components/servicesInnerPage/ServicesWhyWorkWithUs";
import ServicesBenefitWorkingWithUs from "../../src/components/servicesInnerPage/ServicesBenefitWorkingWithUs";
// import GetaQuote from "../../src/components/home/GetaQuoteSection";
// import Subscribe from "../../src/components/layout/Subscribe";
import OtherServices from "../../src/components/servicesInnerPage/OtherServices";


const Services = ({ data }) => {
    const router = useRouter();

    // If the page is not yet generated, this will be displayed
    // initially until getStaticProps() finishes running

    const override = css`
    border-color: #ea8808;
    height: 70px;
    width: 8px;
    radius: 4px;
    margin: 4px;
    `;
    let [loading, setLoading] = useState(true);
    let [color, setColor] = useState("#ea8808");

    if (router.isFallback) {

        // Can be a string as well. Need to ensure each key-value pair ends with ;


        return (
            <div className="position-relative vh-100 container-fluid text-center">

                <div className="position-absolute top-50 start-50 translate-middle">
                    <ScaleLoader color={color} loading={loading} css={override} size={150} />
                </div>

            </div>
        )
    }

    return (
        <Layout data={data}>
            {
                !isEmpty(data?.page?.acfServicesIntroSection?.servicesIntroTitle) &&
                <ServicesIntro acfServicesIntroSection={data?.page?.acfServicesIntroSection} />
            }
            {
                !isEmpty(data?.page?.acfServicesFeatureSection?.servicesFeatureTitle) &&
                <ServicesFeatures acfServicesFeatureSection={data?.page?.acfServicesFeatureSection} />
            }
            {
                !isEmpty(data?.page?.acfServicesProcessSection?.serviceProcessTitle) &&
                <ServicesHowItWorks acfServicesProcessSection={data?.page?.acfServicesProcessSection} />
            }
            {
                !isEmpty(data?.page?.acfServicesWhyWorkWithUs?.whyWorkWithUsTitle) &&
                <ServicesWhyWorkWithUs acfServicesWhyWorkWithUs={data?.page?.acfServicesWhyWorkWithUs} />
            }
            {
                !isEmpty(data?.page?.acfOtherServices?.otherServicesSectionTitle) &&
                <OtherServices acfOtherServices={data?.page?.acfOtherServices} />
            }

            {
                !isEmpty(data?.page?.acfServicesBenefitWorkingWithUs?.servicesBenefitWorkingWithUsTitle) &&
                <ServicesBenefitWorkingWithUs acfServicesBenefitWorkingWithUs={data?.page?.acfServicesBenefitWorkingWithUs} bgColor={{ 'color': 'background: rgba(11, 74, 125, 0.05);' }} />
            }


        </Layout>
    )
}

export default Services;

export async function getStaticProps({ params }) {
    const { data } = await client.query({
        query: GET_SERVICE,
        variables: {
            uri: 'services/' + params?.slug.join("/"),
        },
    });



    return {
        props: {
            data: {
                header: data?.header || [],
                menus: {
                    mainMenus: data?.mainMenus?.edges
                },
                footer: data?.footer || [],
                page: data?.servicePage ?? {},
                homePage: data?.homePage ?? {},
                path: params?.slug.join("/"),
            }
        },
        /**
         * Revalidate means that if a new request comes to server, then every 1 sec it will check
         * if the data is changed, if it is changed then it will update the
         * static file inside .next folder with the new data, so that any 'SUBSEQUENT' requests should have updated data.
         */
        revalidate: 1,
    };
}


export async function getStaticPaths() {
    const { data } = await client.query({
        query: GET_SERVICES_URI
    });

    const PathsData = []

    data?.servicePages?.nodes && data?.servicePages?.nodes.map(page => {
        if (!isEmpty(page?.uri)) {
            //console.log('page?.uri', page.uri);
            const slugs = page?.uri?.split('/').filter(pageSlug => pageSlug);
            PathsData.push({ params: { slug: slugs } })
        }
        else {
            PathsData.push({ params: { slug: "404" } })
        }
    })


    return {
        paths: PathsData,
        fallback: true
    }

}
