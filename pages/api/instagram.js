import fetch from 'isomorphic-unfetch';

export default async (req, res) => {

    const APIURL = process.env.NEXT_PUBLIC_WORDPRESS_SITE_URL;
    const INSTAGRAMTOKEN = process.env.INSTAGRAMTOKEN;
    
    const response = await fetch(
      `https://graph.instagram.com/me/media?fields=id,media_type,media_url,username,timestamp,permalink,caption_text,thumbnail_url&access_token=${INSTAGRAMTOKEN}`
    ).then( response =>  response.json() );
    return res.json({message: response});
    
};
