import fetch from 'isomorphic-unfetch';
import FormData from 'form-data'

export default async (req, res) => {
    const { fullName } = req.body;
    const { tvSize } = req.body;
    const { wallType } = req.body;
    const { hiddenCables } = req.body;
    const { isThis } = req.body;
    const { haveBracket } = req.body;
    const { whatBracket } = req.body;
    const { suburb } = req.body;
    const { phoneNumber } = req.body;
    const { additionalInfo } = req.body;
    const { email } = req.body;
    const { captchaToken } = req.body;

    if (!email) {
        return res.status(400).json({ error: 'Email is required' });
    }

    try {

        const APIURL = process.env.NEXT_PUBLIC_WORDPRESS_SITE_URL;

        var bodyFormData = new FormData();
        wallType?.map((value, key) => {
            if (value?.isChecked === true) {
                bodyFormData.append("wallType[]", value.value);
            }
        })
        hiddenCables?.map((value, key) => {
            if (value?.isChecked === true) {
                bodyFormData.append("hiddenCables[]", value.value);
            }
        })
        isThis?.map((value, key) => {
            if (value?.isChecked === true) {
                bodyFormData.append("isThis[]", value.value);
            }
        })
        haveBracket?.map((value, key) => {
            if (value?.isChecked === true) {
                bodyFormData.append("haveBracket[]", value.value);
            }
        })
        whatBracket?.map((value, key) => {
            if (value?.isChecked === true) {
                bodyFormData.append("whatBracket[]", value.value);
            }
        })

        bodyFormData.append("tvSize", tvSize);
        //bodyFormData.append("wallType", wallType);
        //bodyFormData.append("hiddenCables", hiddenCables);
        //bodyFormData.append("haveBracket", haveBracket);
        //bodyFormData.append("whatBracket", whatBracket);
        bodyFormData.append("suburb", suburb);
        bodyFormData.append("phoneNumber", phoneNumber);
        bodyFormData.append("additionalInfo", additionalInfo);
        bodyFormData.append("yourEmail", email);
        bodyFormData.append("fullName", fullName);




        const captchaResponse = await fetch(
            `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.GOOGLE_CAPTCHA_SECRET_KEY}&response=${captchaToken}`,

        ).then(function (captchaResponse) {
            return captchaResponse.json();
        });;

        if (captchaResponse.success === false) {
            return res.status(400).json({
                error: `There was an error submitting the contact form.`,
            });

        }


        const response = await fetch(
            `${APIURL}/wp-json/contact-form-7/v1/contact-forms/1216/feedback`,
            {
                body: bodyFormData,
                method: 'POST'
            }
        ).then(function (response) {
            return response.json();
        });



        if (response.status >= 400) {
            return res.status(400).json({
                error: `There was an error submitting the contact form.`,
            });
        }

        //console.log(response);

        return res.status(201).json({ error: '', message: response });
    } catch (error) {
        return res.status(500).json({ error: error.message || error.toString() });
    }
};
