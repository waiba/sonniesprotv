import fetch from 'isomorphic-unfetch';
import FormData from 'form-data'

export default async (req, res) => {
  const { email } = req.body;
  const { fullName } = req.body;
  const { phoneNumber } = req.body;
  const { category } = req.body;
  const { contactMessage } = req.body;
  const { captchaToken } = req.body;

  

  if (!email) {
    return res.status(400).json({ error: 'Email is required' });
  }

  try {
    const APIURL = process.env.NEXT_PUBLIC_WORDPRESS_SITE_URL;

    var bodyFormData = new FormData();
    bodyFormData.append("your-name", fullName);
    bodyFormData.append("your-email", email);
    bodyFormData.append("your-phone", phoneNumber)    
    bodyFormData.append("category", category)
    bodyFormData.append("your-message", contactMessage);


    const captchaResponse = await fetch(
        `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.GOOGLE_CAPTCHA_SECRET_KEY}&response=${captchaToken}`,
        
    ).then(function(captchaResponse) {
      return captchaResponse.json();
    });;

    if(captchaResponse.success === false){
      return res.status(400).json({
        error: `There was an error submitting the contact form.`,
      });

    }

    const response = await fetch(
      `${APIURL}/wp-json/contact-form-7/v1/contact-forms/5/feedback`,
      {
        body: bodyFormData,
        method: 'POST'
      }
    ).then(function(response) {
        return response.json();
      });

    

    if (response.status >= 400) {
      return res.status(400).json({
        error: `There was an error submitting the contact form.`,
      });
    }

    //console.log(response);

    return res.status(201).json({ error: '', message: response });
  } catch (error) {
    return res.status(500).json({ error: error.message || error.toString() });
  }
};
