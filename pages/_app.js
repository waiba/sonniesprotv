import { ApolloProvider } from "@apollo/client"
import client from "../src/apollo/client"
//import 'bootstrap/dist/css/bootstrap.css'
//import '../node_modules/reactstrap/dist/reactstrap'
import '../styles/bootstrap.min.css'
import '../styles/block-library/style.min.css'
import '../styles/nprogress.css'
import '../styles/styles.css'

function MyApp({ Component, pageProps }) {
  //return <Component {...pageProps} />
  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
      <div id="fb-root"></div>
      <div className="fb-customerchat"
        attribution="setup_tool"
        page_id="103218811536721"
        theme_color="#0B4A7D">
      </div>
    </ApolloProvider>
  )
}

export default MyApp
