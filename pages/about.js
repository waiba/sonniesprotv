import client from "../src/apollo/client";
//import AboutLayout from "../src/components/layout/about";
//import Head from 'next/head'
import { GET_ABOUT } from "../src/queries/pages/get-about";
import { isEmpty } from 'lodash'
import Layout from "../src/components/layout";
import WhoWeAre from "../src/components/about/WhoWeAre";
import OurMission from "../src/components/about/OurMission";
import AboutpageWhyChooseUs from "../src/components/about/AboutpageWhyChooseUs";
import OurCoreValues from "../src/components/about/OurCoreValues";

export default function About({ data }) {

  return (
    <Layout data={data}>
      {
        !isEmpty(data?.page?.acfAboutPageWhoWeAre) &&
        <WhoWeAre acfWhoWeAre={data?.page?.acfAboutPageWhoWeAre} />
      }
      {
        !isEmpty(data?.page?.acfAboutPageOurMission) &&
        <OurMission acfOurMisson={data?.page?.acfAboutPageOurMission} />
      }
      {
        !isEmpty(data?.page?.acfAboutPageWhyChooseUs) &&
        <AboutpageWhyChooseUs acfAboutPageWhyChooseUs={data?.page?.acfAboutPageWhyChooseUs} />
      }
      {
        !isEmpty(data?.page?.acfAboutPageOurCoreValues) &&
        <OurCoreValues acfAboutOurCoreValues={data?.page?.acfAboutPageOurCoreValues} />
      }
    </Layout>
  )
}


export async function getStaticProps(context) {

  const { data, loading, networkStatus } = await client.query({
    query: GET_ABOUT
  });

  return {
    props: {
      data: {
        header: data?.header || [],
        menus: {
          mainMenus: data?.mainMenus?.edges
        },
        page: data?.page || {},
        homePage: data?.homePage ?? {},
        footer: data?.footer || []
      }
    },
    revalidate: 1 // incremental static regeneration time in seconds
  }
}
