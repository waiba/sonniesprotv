import client from "../src/apollo/client";
import ContactLayout from "../src/components/layout/contact";
//import Head from 'next/head'
import { GET_CONTACT } from "../src/queries/pages/get-contact";
import { isEmpty } from 'lodash'
import Layout from "../src/components/layout";
import ContactBody from "../src/components/contact/ContactBody";
export default function Contact({data}) {
  return (
    <Layout data={data}>
      {
        !isEmpty(data?.page?.acfContactUsFields) && 
        <ContactBody contactForm={data?.page?.acfContactUsFields} homeContactInfo={data?.homePage?.acfContactInfo}/>
      }
    </Layout>
  )
}


export async function getStaticProps(context) {

  const { data, loading, networkStatus } = await client.query({
    query: GET_CONTACT
  });

  return {
    props: {
      data: {
        header: data?.header || [],
        menus:{
          mainMenus: data?.mainMenus?.edges
        },
        page: data?.page || {},
        homePage: data?.homePage ?? {},
        footer: data?.footer || []
      }
    },
    revalidate: 1 // incremental static regeneration time in seconds
  }
}
