import client from "../src/apollo/client";
import Layout from "../src/components/layout";
//import Head from 'next/head'
import { GET_HOME } from "../src/queries/pages/get-home";
//import { isEmpty } from 'lodash'
export default function Index({ data }) {
  return (
    <Layout data={data}>
    </Layout>
  )
}


export async function getStaticProps(context) {

  const { data, loading, networkStatus } = await client.query({
    query: GET_HOME
  });

  return {
    props: {
      data: {
        header: data?.header || [],
        menus: {
          mainMenus: data?.mainMenus?.edges
        },
        page: data?.page ?? {},
        testimonials: data?.testimonials ?? {},
        associates: data?.ourassociates?.edges ?? {},
        services: data?.services?.edges ?? {},
        footer: data?.footer || []
      }
    },
    revalidate: 1 // incremental static regeneration time in seconds
  }
}
