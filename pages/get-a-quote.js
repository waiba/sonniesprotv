import client from "../src/apollo/client";
//import ContactLayout from "../src/components/layout/contact";
//import Head from 'next/head'
import { GET_A_QUOTE } from "../src/queries/pages/get-a-quote";
import { isEmpty } from 'lodash'
import Layout from "../src/components/layout";
//import ContactBody from "../src/components/contact/ContactBody";
import GetaQuoteBody from "../src/components/get-a-quote/get-a-quote-body";
export default function GetaQuote({ data }) {
    return (
        <Layout data={data}>
            {
                !isEmpty(data?.page?.acfContactUsFields) &&
                <GetaQuoteBody contactForm={data?.page?.acfContactUsFields} homeContactInfo={data?.homePage?.acfContactInfo} />
            }
        </Layout>
    )
}


export async function getStaticProps(context) {

    const { data, loading, networkStatus } = await client.query({
        query: GET_A_QUOTE
    });

    return {
        props: {
            data: {
                header: data?.header || [],
                menus: {
                    mainMenus: data?.mainMenus?.edges
                },
                page: data?.page || {},
                homePage: data?.homePage ?? {},
                footer: data?.footer || []
            }
        },
        revalidate: 1 // incremental static regeneration time in seconds
    }
}
