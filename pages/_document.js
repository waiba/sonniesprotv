import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {

  render() {
    return (
      <Html lang="en">
        <Head />
        <body>
          <Main />
          <NextScript />
          {/* <script dangerouslySetInnerHTML={{__html: `
            
          jQuery(document).ready(function($) {
            $('.instaslider-nr-3').pllexislider({
              animation: 'slide',
              slideshowSpeed: 7000,
              itemWidth:210, itemMargin: 24, minItems:2, maxItems:5,directionNav: true,
              controlNav: false,
              prevText: '',
              nextText: '',
            });
          }); 

            `}} defer></script> */}

          <script dangerouslySetInnerHTML={{
            __html: `
              
              window.fbAsyncInit = function() {
                FB.init({
                  xfbml            : true,
                  version          : 'v9.0'
                });
              };
      
              (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));`}}  ></script>


          {/* <script id="mcjs" dangerouslySetInnerHTML={{
              __html: `!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/65a0f5c152a71183326424a6d/33dcf075b7407c44e21f82e4b.js");`}}></script> */}
          {/* <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/65a0f5c152a71183326424a6d/33dcf075b7407c44e21f82e4b.js");</script> */}
          {/* <!-- Global site tag (gtag.js) - Google Analytics --> */}

        </body>

      </Html>
    )
  }
}

export default MyDocument