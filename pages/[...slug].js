import { useState } from "react";
import { css } from "@emotion/react";
import ScaleLoader from "react-spinners/ScaleLoader";
import { isEmpty } from 'lodash'
import client from "../src/apollo/client";
import { GET_PAGES_URI } from "../src/queries/pages/get-pages";
import { GET_PAGE } from "../src/queries/pages/get-page";
import { useRouter } from 'next/router'
import Layout from '../src/components/layout';
import NotFound from './404';

const Pages = ({ data }) => {

    const router = useRouter();
    //console.log('data', router);

    // If the page is not yet generated, this will be displayed
    // initially until getStaticProps() finishes running

    const override = css`
    border-color: #ea8808;
    height: 70px;
    width: 8px;
    radius: 4px;
    margin: 4px;
    `;
    let [loading, setLoading] = useState(true);
    let [color, setColor] = useState("#ea8808");

    if (router.isFallback) {
        return (
            <div className="position-relative vh-100 container-fluid text-center">

                <div className="position-absolute top-50 start-50 translate-middle">
                    <ScaleLoader color={color} loading={loading} css={override} size={150} />
                </div>

            </div>
        )
    }

    if (!isEmpty(data?.page)) {
        return (
            <Layout data={data}>

            </Layout>
        )
    } else {
        return (
            <NotFound data={data} />
        )
    }


}

export default Pages;


export async function getStaticProps({ params }) {
    const { data } = await client.query({
        query: GET_PAGE,
        variables: {
            uri: params?.slug.join("/"),
        },
    });

    return {
        props: {
            data: {
                header: data?.header || [],
                menus: {
                    mainMenus: data?.mainMenus?.edges
                },
                footer: data?.footer || [],
                page: data?.page ?? {},
                homePage: data?.homePage ?? {},
                acfFaqs: data?.acfFaqs ?? {},
                path: params?.slug.join("/"),
            }
        },
        /**
         * Revalidate means that if a new request comes to server, then every 1 sec it will check
         * if the data is changed, if it is changed then it will update the
         * static file inside .next folder with the new data, so that any 'SUBSEQUENT' requests should have updated data.
         */
        revalidate: 1,
    };
}


export async function getStaticPaths() {
    const { data } = await client.query({
        query: GET_PAGES_URI
    });

    //console.log(data);

    const PathsData = []

    data?.pages?.nodes && data?.pages?.nodes.map(page => {
        if (!isEmpty(page?.uri)) {
            const slugs = page?.uri?.split('/').filter(pageSlug => pageSlug);
            if (!isEmpty(slugs) && !slugs.includes('about') && !slugs.includes('services') && !slugs.includes('contact-us') && !slugs.includes('testimonials') && !slugs.includes('get-a-quote')) {
                PathsData.push({ params: { slug: slugs } })
            }
        }
        else {
            PathsData.push({ params: { slug: ["404"] } })
        }
    })


    return {
        paths: PathsData,
        fallback: true
    }

}