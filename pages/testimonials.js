import client from "../src/apollo/client";
//import Head from 'next/head'
import { GET_TESTIMONIALS } from "../src/queries/pages/get-testimonials";
import { isEmpty } from 'lodash'
import TestimonialsLayout from "../src/components/layout/testimonials";
import Layout from "../src/components/layout";
import TestimonialsBody from "../src/components/testimonials";
export default function Testimonials({data}) {
  return (
    <Layout data={data}>
      {
        !isEmpty(data?.testimonials) && 
          <TestimonialsBody TestimonialsBody={data?.testimonials}/>
      }
    </Layout>
  )
}


export async function getStaticProps(context) {

  const { data, loading, networkStatus } = await client.query({
    query: GET_TESTIMONIALS
  });

  return {
    props: {
      data: {
        header: data?.header || [],
        menus:{
          mainMenus: data?.mainMenus?.edges
        },
        page: data?.page || {},
        homePage: data?.homePage ?? {},
        testimonials: data?.testimonials ?? {},
        footer: data?.footer || []
      }
    },
    revalidate: 1 // incremental static regeneration time in seconds
  }
}
